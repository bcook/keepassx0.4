/****************************************************************************
** Meta object code from reading C++ file 'CustomizeDetailViewDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/CustomizeDetailViewDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'CustomizeDetailViewDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CustomizeDetailViewDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   42,   42,   42, 0x08,
      43,   42,   42,   42, 0x08,
      61,   42,   42,   42, 0x08,
      82,   42,   42,   42, 0x08,
      99,   42,   42,   42, 0x08,
     117,   42,   42,   42, 0x08,
     136,   42,   42,   42, 0x08,
     154,   42,   42,   42, 0x08,
     167,   42,   42,   42, 0x08,
     176,   42,   42,   42, 0x08,
     187,  222,   42,   42, 0x08,
     229,   42,   42,   42, 0x08,
     256,   42,   42,   42, 0x08,
     282,  300,   42,   42, 0x08,
     306,  333,   42,   42, 0x08,
     338,   42,   42,   42, 0x28,

       0        // eod
};

static const char qt_meta_stringdata_CustomizeDetailViewDialog[] = {
    "CustomizeDetailViewDialog\0OnBtnBold(bool)\0"
    "\0OnBtnItalic(bool)\0OnBtnUnderline(bool)\0"
    "OnBtnAlignLeft()\0OnBtnAlignRight()\0"
    "OnBtnAlignCenter()\0OnBtnAlignBlock()\0"
    "OnBtnColor()\0OnSave()\0OnCancel()\0"
    "OnRestoreDefault(QAbstractButton*)\0"
    "button\0OnInsertTemplate(QAction*)\0"
    "OnCursorPositionChanged()\0OnTabChanged(int)\0"
    "index\0OnFontSizeChanged(QString)\0text\0"
    "OnFontSizeChanged()\0"
};

void CustomizeDetailViewDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CustomizeDetailViewDialog *_t = static_cast<CustomizeDetailViewDialog *>(_o);
        switch (_id) {
        case 0: _t->OnBtnBold((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->OnBtnItalic((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->OnBtnUnderline((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->OnBtnAlignLeft(); break;
        case 4: _t->OnBtnAlignRight(); break;
        case 5: _t->OnBtnAlignCenter(); break;
        case 6: _t->OnBtnAlignBlock(); break;
        case 7: _t->OnBtnColor(); break;
        case 8: _t->OnSave(); break;
        case 9: _t->OnCancel(); break;
        case 10: _t->OnRestoreDefault((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 11: _t->OnInsertTemplate((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 12: _t->OnCursorPositionChanged(); break;
        case 13: _t->OnTabChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->OnFontSizeChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->OnFontSizeChanged(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CustomizeDetailViewDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CustomizeDetailViewDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CustomizeDetailViewDialog,
      qt_meta_data_CustomizeDetailViewDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CustomizeDetailViewDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CustomizeDetailViewDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CustomizeDetailViewDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CustomizeDetailViewDialog))
        return static_cast<void*>(const_cast< CustomizeDetailViewDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int CustomizeDetailViewDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
