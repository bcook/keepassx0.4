/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KeepassMainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      40,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      18,   33,   33,   33, 0x05,

 // slots: signature, parameters, type, tag, flags
      34,   33,   33,   33, 0x0a,
      54,   72,   33,   33, 0x0a,
      81,   33,   33,   33, 0x08,
      96,   33,   33,   33, 0x08,
     109,   33,   33,   33, 0x08,
     123,   33,  136,   33, 0x08,
     141,   33,  136,   33, 0x08,
     156,   33,   33,   33, 0x08,
     173,   33,   33,   33, 0x08,
     191,   33,   33,   33, 0x08,
     204,  234,   33,   33, 0x08,
     241,   33,   33,   33, 0x08,
     252,   33,   33,   33, 0x08,
     268,   33,   33,   33, 0x08,
     292,   33,   33,   33, 0x08,
     321,   33,   33,   33, 0x08,
     351,   33,   33,   33, 0x08,
     381,   33,   33,   33, 0x08,
     411,   33,   33,   33, 0x08,
     450,   33,   33,   33, 0x08,
     466,   33,   33,   33, 0x08,
     494,   33,   33,   33, 0x08,
     526,   33,   33,   33, 0x08,
     543,   33,   33,   33, 0x08,
     562,   33,   33,   33, 0x08,
     584,   33,   33,   33, 0x08,
     613,   33,   33,   33, 0x08,
     627,   33,   33,   33, 0x08,
     644,   33,   33,   33, 0x08,
     666,   33,   33,   33, 0x08,
     697,   33,   33,   33, 0x08,
     751,   33,   33,   33, 0x08,
     767,   33,   33,   33, 0x08,
     786,   33,   33,   33, 0x08,
     805,  834,   33,   33, 0x08,
     838,   33,   33,   33, 0x08,
     852,   33,   33,   33, 0x08,
     872,  901,   33,   33, 0x08,
     909,   33,   33,   33, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KeepassMainWindow[] = {
    "KeepassMainWindow\0entryChanged()\0\0"
    "OnUnLockWorkspace()\0openFile(QString)\0"
    "filename\0OnFileNewKdb()\0OnFileOpen()\0"
    "OnFileClose()\0OnFileSave()\0bool\0"
    "OnFileSaveAs()\0OnFileSettings()\0"
    "OnFileChangeKey()\0OnFileExit()\0"
    "OnBookmarkTriggered(QAction*)\0action\0"
    "OnSearch()\0OnGroupSearch()\0"
    "OnViewShowToolbar(bool)\0"
    "OnViewShowEntryDetails(bool)\0"
    "OnViewToolbarIconSize16(bool)\0"
    "OnViewToolbarIconSize22(bool)\0"
    "OnViewToolbarIconSize28(bool)\0"
    "OnGroupSelectionChanged(IGroupHandle*)\0"
    "OnQuickSearch()\0OnColumnVisibilityChanged()\0"
    "OnUsernPasswVisibilityChanged()\0"
    "OnFileModified()\0OnExtrasSettings()\0"
    "OnExtrasPasswordGen()\0"
    "OnExtrasShowExpiredEntries()\0OnHelpAbout()\0"
    "OnHelpHandbook()\0OnShowSearchResults()\0"
    "OnEntryChanged(SelectionState)\0"
    "OnSysTrayActivated(QSystemTrayIcon::ActivationReason)\0"
    "restoreWindow()\0OnImport(QAction*)\0"
    "OnExport(QAction*)\0OnDetailViewUrlClicked(QUrl)\0"
    "url\0OnLockClose()\0OnInactivityTimer()\0"
    "OnShutdown(QSessionManager&)\0manager\0"
    "loadColumnVisibility()\0"
};

void KeepassMainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeepassMainWindow *_t = static_cast<KeepassMainWindow *>(_o);
        switch (_id) {
        case 0: _t->entryChanged(); break;
        case 1: _t->OnUnLockWorkspace(); break;
        case 2: _t->openFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->OnFileNewKdb(); break;
        case 4: _t->OnFileOpen(); break;
        case 5: _t->OnFileClose(); break;
        case 6: { bool _r = _t->OnFileSave();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 7: { bool _r = _t->OnFileSaveAs();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 8: _t->OnFileSettings(); break;
        case 9: _t->OnFileChangeKey(); break;
        case 10: _t->OnFileExit(); break;
        case 11: _t->OnBookmarkTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 12: _t->OnSearch(); break;
        case 13: _t->OnGroupSearch(); break;
        case 14: _t->OnViewShowToolbar((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: _t->OnViewShowEntryDetails((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->OnViewToolbarIconSize16((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->OnViewToolbarIconSize22((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->OnViewToolbarIconSize28((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->OnGroupSelectionChanged((*reinterpret_cast< IGroupHandle*(*)>(_a[1]))); break;
        case 20: _t->OnQuickSearch(); break;
        case 21: _t->OnColumnVisibilityChanged(); break;
        case 22: _t->OnUsernPasswVisibilityChanged(); break;
        case 23: _t->OnFileModified(); break;
        case 24: _t->OnExtrasSettings(); break;
        case 25: _t->OnExtrasPasswordGen(); break;
        case 26: _t->OnExtrasShowExpiredEntries(); break;
        case 27: _t->OnHelpAbout(); break;
        case 28: _t->OnHelpHandbook(); break;
        case 29: _t->OnShowSearchResults(); break;
        case 30: _t->OnEntryChanged((*reinterpret_cast< SelectionState(*)>(_a[1]))); break;
        case 31: _t->OnSysTrayActivated((*reinterpret_cast< QSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 32: _t->restoreWindow(); break;
        case 33: _t->OnImport((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 34: _t->OnExport((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 35: _t->OnDetailViewUrlClicked((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 36: _t->OnLockClose(); break;
        case 37: _t->OnInactivityTimer(); break;
        case 38: _t->OnShutdown((*reinterpret_cast< QSessionManager(*)>(_a[1]))); break;
        case 39: _t->loadColumnVisibility(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KeepassMainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KeepassMainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_KeepassMainWindow,
      qt_meta_data_KeepassMainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KeepassMainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KeepassMainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KeepassMainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeepassMainWindow))
        return static_cast<void*>(const_cast< KeepassMainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int KeepassMainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 40)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 40;
    }
    return _id;
}

// SIGNAL 0
void KeepassMainWindow::entryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
