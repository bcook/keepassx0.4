/****************************************************************************
** Meta object code from reading C++ file 'EditEntryDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/EditEntryDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EditEntryDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CEditEntryDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      14,   42,   42,   42, 0x08,
      43,   42,   42,   42, 0x08,
      66,   42,   42,   42, 0x08,
      91,   42,   42,   42, 0x08,
     115,   42,   42,   42, 0x08,
     132,   42,   42,   42, 0x08,
     149,   42,   42,   42, 0x08,
     167,   42,   42,   42, 0x08,
     188,   42,   42,   42, 0x08,
     207,   42,   42,   42, 0x08,
     223,  258,   42,   42, 0x08,
     264,   42,   42,   42, 0x08,
     280,   42,   42,   42, 0x08,
     293,  318,   42,   42, 0x08,
     325,  318,   42,   42, 0x08,
     346,   42,   42,   42, 0x08,
     356,   42,   42,   42, 0x08,
     378,   42,   42,   42, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CEditEntryDlg[] = {
    "CEditEntryDlg\0OnTitleTextChanged(QString)\0"
    "\0OnPasswordwLostFocus()\0"
    "OnPasswordwTextChanged()\0"
    "OnPasswordTextChanged()\0ChangeEchoMode()\0"
    "OnButtonCancel()\0OnNewAttachment()\0"
    "OnDeleteAttachment()\0OnSaveAttachment()\0"
    "OnButtonGenPw()\0OnCheckBoxExpiresNeverChanged(int)\0"
    "state\0OnButtonIcons()\0OnButtonOK()\0"
    "OnExpirePreset(QAction*)\0action\0"
    "OnCalendar(QAction*)\0OnClose()\0"
    "OnCustomizeSequence()\0OnSelectTarget()\0"
};

void CEditEntryDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CEditEntryDlg *_t = static_cast<CEditEntryDlg *>(_o);
        switch (_id) {
        case 0: _t->OnTitleTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->OnPasswordwLostFocus(); break;
        case 2: _t->OnPasswordwTextChanged(); break;
        case 3: _t->OnPasswordTextChanged(); break;
        case 4: _t->ChangeEchoMode(); break;
        case 5: _t->OnButtonCancel(); break;
        case 6: _t->OnNewAttachment(); break;
        case 7: _t->OnDeleteAttachment(); break;
        case 8: _t->OnSaveAttachment(); break;
        case 9: _t->OnButtonGenPw(); break;
        case 10: _t->OnCheckBoxExpiresNeverChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: _t->OnButtonIcons(); break;
        case 12: _t->OnButtonOK(); break;
        case 13: _t->OnExpirePreset((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 14: _t->OnCalendar((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 15: _t->OnClose(); break;
        case 16: _t->OnCustomizeSequence(); break;
        case 17: _t->OnSelectTarget(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CEditEntryDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CEditEntryDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CEditEntryDlg,
      qt_meta_data_CEditEntryDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CEditEntryDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CEditEntryDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CEditEntryDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CEditEntryDlg))
        return static_cast<void*>(const_cast< CEditEntryDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int CEditEntryDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
