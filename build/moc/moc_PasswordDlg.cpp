/****************************************************************************
** Meta object code from reading C++ file 'PasswordDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/PasswordDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PasswordDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_PasswordDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   22,   22,   22, 0x08,
      23,   22,   22,   22, 0x08,
      34,   22,   22,   22, 0x08,
      51,   22,   22,   22, 0x08,
      66,   22,   22,   22, 0x08,
      81,   22,   22,   22, 0x08,
      96,   22,   22,   22, 0x08,
     124,  154,   22,   22, 0x08,
     161,   22,   22,   22, 0x08,
     183,  193,   22,   22, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_PasswordDialog[] = {
    "PasswordDialog\0OnOK()\0\0OnCancel()\0"
    "OnButtonBrowse()\0OnButtonQuit()\0"
    "OnGenKeyFile()\0OnButtonBack()\0"
    "ChangeEchoModeDatabaseKey()\0"
    "OnBookmarkTriggered(QAction*)\0action\0"
    "OnCheckBoxesChanged()\0done(int)\0r\0"
};

void PasswordDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        PasswordDialog *_t = static_cast<PasswordDialog *>(_o);
        switch (_id) {
        case 0: _t->OnOK(); break;
        case 1: _t->OnCancel(); break;
        case 2: _t->OnButtonBrowse(); break;
        case 3: _t->OnButtonQuit(); break;
        case 4: _t->OnGenKeyFile(); break;
        case 5: _t->OnButtonBack(); break;
        case 6: _t->ChangeEchoModeDatabaseKey(); break;
        case 7: _t->OnBookmarkTriggered((*reinterpret_cast< QAction*(*)>(_a[1]))); break;
        case 8: _t->OnCheckBoxesChanged(); break;
        case 9: _t->done((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData PasswordDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PasswordDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_PasswordDialog,
      qt_meta_data_PasswordDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PasswordDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PasswordDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PasswordDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PasswordDialog))
        return static_cast<void*>(const_cast< PasswordDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int PasswordDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
