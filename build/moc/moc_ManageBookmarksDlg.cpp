/****************************************************************************
** Meta object code from reading C++ file 'ManageBookmarksDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/ManageBookmarksDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ManageBookmarksDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ManageBookmarksDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   33,   33,   33, 0x08,
      34,   33,   33,   33, 0x08,
      49,   33,   33,   33, 0x08,
      66,   33,   33,   33, 0x08,
      79,   33,   33,   33, 0x08,
      94,   33,   33,   33, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_ManageBookmarksDlg[] = {
    "ManageBookmarksDlg\0OnButtonAdd()\0\0"
    "OnButtonEdit()\0OnButtonDelete()\0"
    "OnButtonUp()\0OnButtonDown()\0"
    "edit(QListWidgetItem*)\0"
};

void ManageBookmarksDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ManageBookmarksDlg *_t = static_cast<ManageBookmarksDlg *>(_o);
        switch (_id) {
        case 0: _t->OnButtonAdd(); break;
        case 1: _t->OnButtonEdit(); break;
        case 2: _t->OnButtonDelete(); break;
        case 3: _t->OnButtonUp(); break;
        case 4: _t->OnButtonDown(); break;
        case 5: _t->edit((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ManageBookmarksDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ManageBookmarksDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ManageBookmarksDlg,
      qt_meta_data_ManageBookmarksDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ManageBookmarksDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ManageBookmarksDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ManageBookmarksDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ManageBookmarksDlg))
        return static_cast<void*>(const_cast< ManageBookmarksDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int ManageBookmarksDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
