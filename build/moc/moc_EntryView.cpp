/****************************************************************************
** Meta object code from reading C++ file 'EntryView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/lib/EntryView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'EntryView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KeepassEntryView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   32,   32,   32, 0x05,
      33,   32,   32,   32, 0x05,
      66,  117,   32,   32, 0x05,
     136,  158,   32,   32, 0x05,

 // slots: signature, parameters, type, tag, flags
     175,  205,   32,   32, 0x08,
     211,   32,   32,   32, 0x08,
     233,  272,   32,   32, 0x08,
     274,  272,   32,   32, 0x08,
     314,   32,   32,   32, 0x08,
     327,   32,   32,   32, 0x08,
     344,   32,   32,   32, 0x08,
     358,   32,   32,   32, 0x08,
     382,   32,   32,   32, 0x08,
     406,   32,   32,   32, 0x08,
     420,   32,   32,   32, 0x08,
     441,   32,   32,   32, 0x08,
     456,   32,   32,   32, 0x08,
     472,   32,   32,   32, 0x08,
     491,   32,   32,   32, 0x08,
     509,   32,   32,   32, 0x08,
     525,   32,   32,   32, 0x08,
     541,   32,   32,   32, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KeepassEntryView[] = {
    "KeepassEntryView\0fileModified()\0\0"
    "selectionChanged(SelectionState)\0"
    "requestCreateGroup(QString,quint32,GroupViewItem*)\0"
    "title,image,parent\0viewModeChanged(bool)\0"
    "searchResultMode\0OnGroupChanged(IGroupHandle*)\0"
    "group\0OnShowSearchResults()\0"
    "OnEntryActivated(QTreeWidgetItem*,int)\0"
    ",\0OnEntryDblClicked(QTreeWidgetItem*,int)\0"
    "OnNewEntry()\0OnItemsChanged()\0"
    "updateIcons()\0OnUsernameToClipboard()\0"
    "OnPasswordToClipboard()\0OnEditEntry()\0"
    "OnClipboardTimeOut()\0OnCloneEntry()\0"
    "OnDeleteEntry()\0OnSaveAttachment()\0"
    "removeDragItems()\0OnEditOpenUrl()\0"
    "OnEditCopyUrl()\0resizeColumns()\0"
};

void KeepassEntryView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeepassEntryView *_t = static_cast<KeepassEntryView *>(_o);
        switch (_id) {
        case 0: _t->fileModified(); break;
        case 1: _t->selectionChanged((*reinterpret_cast< SelectionState(*)>(_a[1]))); break;
        case 2: _t->requestCreateGroup((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2])),(*reinterpret_cast< GroupViewItem*(*)>(_a[3]))); break;
        case 3: _t->viewModeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->OnGroupChanged((*reinterpret_cast< IGroupHandle*(*)>(_a[1]))); break;
        case 5: _t->OnShowSearchResults(); break;
        case 6: _t->OnEntryActivated((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 7: _t->OnEntryDblClicked((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: _t->OnNewEntry(); break;
        case 9: _t->OnItemsChanged(); break;
        case 10: _t->updateIcons(); break;
        case 11: _t->OnUsernameToClipboard(); break;
        case 12: _t->OnPasswordToClipboard(); break;
        case 13: _t->OnEditEntry(); break;
        case 14: _t->OnClipboardTimeOut(); break;
        case 15: _t->OnCloneEntry(); break;
        case 16: _t->OnDeleteEntry(); break;
        case 17: _t->OnSaveAttachment(); break;
        case 18: _t->removeDragItems(); break;
        case 19: _t->OnEditOpenUrl(); break;
        case 20: _t->OnEditCopyUrl(); break;
        case 21: _t->resizeColumns(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KeepassEntryView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KeepassEntryView::staticMetaObject = {
    { &QTreeWidget::staticMetaObject, qt_meta_stringdata_KeepassEntryView,
      qt_meta_data_KeepassEntryView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KeepassEntryView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KeepassEntryView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KeepassEntryView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeepassEntryView))
        return static_cast<void*>(const_cast< KeepassEntryView*>(this));
    return QTreeWidget::qt_metacast(_clname);
}

int KeepassEntryView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void KeepassEntryView::fileModified()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void KeepassEntryView::selectionChanged(SelectionState _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void KeepassEntryView::requestCreateGroup(QString _t1, quint32 _t2, GroupViewItem * _t3)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void KeepassEntryView::viewModeChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_END_MOC_NAMESPACE
