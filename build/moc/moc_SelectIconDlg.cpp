/****************************************************************************
** Meta object code from reading C++ file 'SelectIconDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/SelectIconDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SelectIconDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CSelectIconDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   27,   27,   27, 0x08,
      28,   27,   27,   27, 0x08,
      41,   27,   27,   27, 0x08,
      52,   27,   27,   27, 0x08,
      63,   27,   27,   27, 0x08,
      75,   27,   27,   27, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CSelectIconDlg[] = {
    "CSelectIconDlg\0OnAddIcon()\0\0OnPickIcon()\0"
    "OnCancel()\0OnDelete()\0OnReplace()\0"
    "OnSelectionChanged(QListWidgetItem*)\0"
};

void CSelectIconDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CSelectIconDlg *_t = static_cast<CSelectIconDlg *>(_o);
        switch (_id) {
        case 0: _t->OnAddIcon(); break;
        case 1: _t->OnPickIcon(); break;
        case 2: _t->OnCancel(); break;
        case 3: _t->OnDelete(); break;
        case 4: _t->OnReplace(); break;
        case 5: _t->OnSelectionChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CSelectIconDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CSelectIconDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CSelectIconDlg,
      qt_meta_data_CSelectIconDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CSelectIconDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CSelectIconDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CSelectIconDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CSelectIconDlg))
        return static_cast<void*>(const_cast< CSelectIconDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int CSelectIconDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
