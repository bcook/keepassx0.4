/****************************************************************************
** Meta object code from reading C++ file 'AddBookmarkDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/AddBookmarkDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'AddBookmarkDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AddBookmarkDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   28,   28,   28, 0x08,
      29,   28,   28,   28, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_AddBookmarkDlg[] = {
    "AddBookmarkDlg\0OnButtonOk()\0\0"
    "OnButtonBrowse()\0"
};

void AddBookmarkDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AddBookmarkDlg *_t = static_cast<AddBookmarkDlg *>(_o);
        switch (_id) {
        case 0: _t->OnButtonOk(); break;
        case 1: _t->OnButtonBrowse(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData AddBookmarkDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AddBookmarkDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_AddBookmarkDlg,
      qt_meta_data_AddBookmarkDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AddBookmarkDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AddBookmarkDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AddBookmarkDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AddBookmarkDlg))
        return static_cast<void*>(const_cast< AddBookmarkDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int AddBookmarkDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
