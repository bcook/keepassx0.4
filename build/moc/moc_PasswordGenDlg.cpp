/****************************************************************************
** Meta object code from reading C++ file 'PasswordGenDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/PasswordGenDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'PasswordGenDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CGenPwDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   28,   28,   28, 0x08,
      29,   28,   28,   28, 0x08,
      40,   28,   28,   28, 0x08,
      51,   28,   28,   28, 0x08,
      69,   28,   28,   28, 0x08,
      98,   28,   28,   28, 0x08,
     113,   28,   28,   28, 0x08,
     134,  160,   28,   28, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CGenPwDialog[] = {
    "CGenPwDialog\0OnGeneratePw()\0\0OnCancel()\0"
    "OnAccept()\0estimateQuality()\0"
    "OnCollectEntropyChanged(int)\0"
    "SwapEchoMode()\0setGenerateEnabled()\0"
    "setAcceptEnabled(QString)\0str\0"
};

void CGenPwDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CGenPwDialog *_t = static_cast<CGenPwDialog *>(_o);
        switch (_id) {
        case 0: _t->OnGeneratePw(); break;
        case 1: _t->OnCancel(); break;
        case 2: _t->OnAccept(); break;
        case 3: _t->estimateQuality(); break;
        case 4: _t->OnCollectEntropyChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->SwapEchoMode(); break;
        case 6: _t->setGenerateEnabled(); break;
        case 7: _t->setAcceptEnabled((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CGenPwDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CGenPwDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CGenPwDialog,
      qt_meta_data_CGenPwDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CGenPwDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CGenPwDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CGenPwDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CGenPwDialog))
        return static_cast<void*>(const_cast< CGenPwDialog*>(this));
    if (!strcmp(_clname, "Ui_GenPwDlg"))
        return static_cast< Ui_GenPwDlg*>(const_cast< CGenPwDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int CGenPwDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
static const uint qt_meta_data_PassCharValidator[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_PassCharValidator[] = {
    "PassCharValidator\0"
};

void PassCharValidator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData PassCharValidator::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject PassCharValidator::staticMetaObject = {
    { &QValidator::staticMetaObject, qt_meta_stringdata_PassCharValidator,
      qt_meta_data_PassCharValidator, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &PassCharValidator::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *PassCharValidator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *PassCharValidator::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_PassCharValidator))
        return static_cast<void*>(const_cast< PassCharValidator*>(this));
    return QValidator::qt_metacast(_clname);
}

int PassCharValidator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QValidator::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
