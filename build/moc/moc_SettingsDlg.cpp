/****************************************************************************
** Meta object code from reading C++ file 'SettingsDlg.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/dialogs/SettingsDlg.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SettingsDlg.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CSettingsDlg[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   24,   24,   24, 0x08,
      25,   24,   24,   24, 0x08,
      32,   24,   24,   24, 0x08,
      46,   24,   24,   24, 0x08,
      57,   24,   24,   24, 0x08,
      68,   24,   24,   24, 0x08,
     100,   24,   24,   24, 0x08,
     119,   24,   24,   24, 0x08,
     140,   24,   24,   24, 0x08,
     166,  195,   24,   24, 0x08,
     203,  195,   24,   24, 0x08,
     226,  195,   24,   24, 0x08,
     255,   24,   24,   24, 0x08,
     278,  300,   24,   24, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CSettingsDlg[] = {
    "CSettingsDlg\0OnCancel()\0\0OnOK()\0"
    "OnTextColor()\0OnColor2()\0OnColor1()\0"
    "OnOtherButton(QAbstractButton*)\0"
    "OnMountDirBrowse()\0OnBrowserCmdBrowse()\0"
    "OnCustomizeEntryDetails()\0"
    "OnInactivityLockChange(bool)\0checked\0"
    "OnAutoSaveToggle(bool)\0"
    "OnAutoSaveChangeToggle(bool)\0"
    "OnBackupDeleteChange()\0OnSelectLanguage(int)\0"
    "index\0"
};

void CSettingsDlg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CSettingsDlg *_t = static_cast<CSettingsDlg *>(_o);
        switch (_id) {
        case 0: _t->OnCancel(); break;
        case 1: _t->OnOK(); break;
        case 2: _t->OnTextColor(); break;
        case 3: _t->OnColor2(); break;
        case 4: _t->OnColor1(); break;
        case 5: _t->OnOtherButton((*reinterpret_cast< QAbstractButton*(*)>(_a[1]))); break;
        case 6: _t->OnMountDirBrowse(); break;
        case 7: _t->OnBrowserCmdBrowse(); break;
        case 8: _t->OnCustomizeEntryDetails(); break;
        case 9: _t->OnInactivityLockChange((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->OnAutoSaveToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->OnAutoSaveChangeToggle((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->OnBackupDeleteChange(); break;
        case 13: _t->OnSelectLanguage((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CSettingsDlg::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CSettingsDlg::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CSettingsDlg,
      qt_meta_data_CSettingsDlg, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CSettingsDlg::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CSettingsDlg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CSettingsDlg::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CSettingsDlg))
        return static_cast<void*>(const_cast< CSettingsDlg*>(this));
    return QDialog::qt_metacast(_clname);
}

int CSettingsDlg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
