/****************************************************************************
** Meta object code from reading C++ file 'FileDialogs.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/lib/FileDialogs.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'FileDialogs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FileDlgHistory[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      15,   40,   56,   56, 0x0a,
      57,   56,   56,   56, 0x0a,
      64,   56,   56,   56, 0x0a,
      71,   56,   56,   56, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FileDlgHistory[] = {
    "FileDlgHistory\0set(QString,QString,int)\0"
    "name,dir,filter\0\0save()\0load()\0clear()\0"
};

void FileDlgHistory::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FileDlgHistory *_t = static_cast<FileDlgHistory *>(_o);
        switch (_id) {
        case 0: _t->set((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 1: _t->save(); break;
        case 2: _t->load(); break;
        case 3: _t->clear(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FileDlgHistory::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FileDlgHistory::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileDlgHistory,
      qt_meta_data_FileDlgHistory, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileDlgHistory::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileDlgHistory::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileDlgHistory::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileDlgHistory))
        return static_cast<void*>(const_cast< FileDlgHistory*>(this));
    return QObject::qt_metacast(_clname);
}

int FileDlgHistory::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
static const uint qt_meta_data_QtStandardFileDialogs[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_QtStandardFileDialogs[] = {
    "QtStandardFileDialogs\0"
};

void QtStandardFileDialogs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData QtStandardFileDialogs::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject QtStandardFileDialogs::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QtStandardFileDialogs,
      qt_meta_data_QtStandardFileDialogs, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &QtStandardFileDialogs::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *QtStandardFileDialogs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *QtStandardFileDialogs::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QtStandardFileDialogs))
        return static_cast<void*>(const_cast< QtStandardFileDialogs*>(this));
    if (!strcmp(_clname, "IFileDialog"))
        return static_cast< IFileDialog*>(const_cast< QtStandardFileDialogs*>(this));
    if (!strcmp(_clname, "org.KeePassX.FileDialogInterface/1.0"))
        return static_cast< IFileDialog*>(const_cast< QtStandardFileDialogs*>(this));
    return QObject::qt_metacast(_clname);
}

int QtStandardFileDialogs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
