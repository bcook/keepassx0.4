/****************************************************************************
** Meta object code from reading C++ file 'GroupView.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/lib/GroupView.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GroupView.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KeepassGroupView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   45,   54,   54, 0x05,
      55,   54,   54,   54, 0x05,
      79,   54,   54,   54, 0x05,
      94,   54,   54,   54, 0x05,

 // slots: signature, parameters, type, tag, flags
     111,  155,   54,   54, 0x0a,
     173,  202,   54,   54, 0x2a,
     214,   54,   54,   54, 0x08,
     254,   54,   54,   54, 0x08,
     270,   54,   54,   54, 0x08,
     283,   54,   54,   54, 0x08,
     299,   54,   54,   54, 0x08,
     313,   54,   54,   54, 0x08,
     327,   54,   54,   54, 0x08,
     349,   54,   54,   54, 0x08,
     382,   54,   54,   54, 0x08,
     416,   54,   54,   54, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_KeepassGroupView[] = {
    "KeepassGroupView\0groupChanged(IGroupHandle*)\0"
    "NewGroup\0\0searchResultsSelected()\0"
    "fileModified()\0entriesDropped()\0"
    "createGroup(QString,quint32,GroupViewItem*)\0"
    "title,image,group\0createGroup(QString,quint32)\0"
    "title,image\0OnCurrentGroupChanged(QTreeWidgetItem*)\0"
    "OnDeleteGroup()\0OnNewGroup()\0"
    "OnNewSubgroup()\0OnEditGroup()\0"
    "updateIcons()\0OnHideSearchResults()\0"
    "OnItemExpanded(QTreeWidgetItem*)\0"
    "OnItemCollapsed(QTreeWidgetItem*)\0"
    "OnSort()\0"
};

void KeepassGroupView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KeepassGroupView *_t = static_cast<KeepassGroupView *>(_o);
        switch (_id) {
        case 0: _t->groupChanged((*reinterpret_cast< IGroupHandle*(*)>(_a[1]))); break;
        case 1: _t->searchResultsSelected(); break;
        case 2: _t->fileModified(); break;
        case 3: _t->entriesDropped(); break;
        case 4: _t->createGroup((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2])),(*reinterpret_cast< GroupViewItem*(*)>(_a[3]))); break;
        case 5: _t->createGroup((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< quint32(*)>(_a[2]))); break;
        case 6: _t->OnCurrentGroupChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 7: _t->OnDeleteGroup(); break;
        case 8: _t->OnNewGroup(); break;
        case 9: _t->OnNewSubgroup(); break;
        case 10: _t->OnEditGroup(); break;
        case 11: _t->updateIcons(); break;
        case 12: _t->OnHideSearchResults(); break;
        case 13: _t->OnItemExpanded((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 14: _t->OnItemCollapsed((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 15: _t->OnSort(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KeepassGroupView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KeepassGroupView::staticMetaObject = {
    { &QTreeWidget::staticMetaObject, qt_meta_stringdata_KeepassGroupView,
      qt_meta_data_KeepassGroupView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KeepassGroupView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KeepassGroupView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KeepassGroupView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KeepassGroupView))
        return static_cast<void*>(const_cast< KeepassGroupView*>(this));
    return QTreeWidget::qt_metacast(_clname);
}

int KeepassGroupView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void KeepassGroupView::groupChanged(IGroupHandle * _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void KeepassGroupView::searchResultsSelected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void KeepassGroupView::fileModified()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void KeepassGroupView::entriesDropped()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
QT_END_MOC_NAMESPACE
