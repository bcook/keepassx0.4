/********************************************************************************
** Form generated from reading UI file 'EditEntryDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITENTRYDLG_H
#define UI_EDITENTRYDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDateTimeEdit>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextEdit>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_EditEntryDialog
{
public:
    QGridLayout *gridLayout;
    QGridLayout *gridLayout1;
    QToolButton *ButtonGenPw;
    QLineEdit *Edit_Password_w;
    QToolButton *ButtonEchoMode;
    QLineEdit *Edit_Password;
    QLabel *textLabel9;
    QLabel *textLabel12;
    QLabel *textLabel4;
    QLabel *textLabel5;
    QLabel *textLabel10;
    QLabel *textLabel7;
    QLabel *textLabel3;
    QLineEdit *Edit_Title;
    QLineEdit *Edit_UserName;
    QLabel *textLabel8;
    QLabel *textLabel6;
    QLabel *textLabel11;
    QTextEdit *Edit_Comment;
    QFrame *line1;
    QHBoxLayout *hboxLayout;
    QLineEdit *Edit_Attachment;
    QToolButton *ButtonOpenAttachment;
    QToolButton *ButtonSaveAttachment;
    QToolButton *ButtonDeleteAttachment;
    QLabel *Label_AttachmentSize;
    QHBoxLayout *hboxLayout1;
    QDateTimeEdit *DateTime_Expire;
    QToolButton *ButtonExpirePresets;
    QCheckBox *CheckBox_ExpiresNever;
    QHBoxLayout *hboxLayout2;
    QProgressBar *Progress_Quali;
    QLabel *Label_Bits;
    QLineEdit *Edit_URL;
    QHBoxLayout *hboxLayout3;
    QComboBox *Combo_Group;
    QSpacerItem *spacerItem;
    QLabel *textLabel1;
    QToolButton *Button_Icons;
    QDialogButtonBox *buttonBox;
    QSpacerItem *spacerItem1;

    void setupUi(QDialog *EditEntryDialog)
    {
        if (EditEntryDialog->objectName().isEmpty())
            EditEntryDialog->setObjectName(QString::fromUtf8("EditEntryDialog"));
        EditEntryDialog->resize(494, 490);
        EditEntryDialog->setSizeGripEnabled(true);
        EditEntryDialog->setModal(true);
        gridLayout = new QGridLayout(EditEntryDialog);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout1 = new QGridLayout();
        gridLayout1->setSpacing(6);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        ButtonGenPw = new QToolButton(EditEntryDialog);
        ButtonGenPw->setObjectName(QString::fromUtf8("ButtonGenPw"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ButtonGenPw->sizePolicy().hasHeightForWidth());
        ButtonGenPw->setSizePolicy(sizePolicy);
        ButtonGenPw->setMinimumSize(QSize(0, 23));
        ButtonGenPw->setMaximumSize(QSize(16777215, 23));

        gridLayout1->addWidget(ButtonGenPw, 1, 1, 1, 1);

        Edit_Password_w = new QLineEdit(EditEntryDialog);
        Edit_Password_w->setObjectName(QString::fromUtf8("Edit_Password_w"));
        QFont font;
        font.setFamily(QString::fromUtf8("Serif"));
        Edit_Password_w->setFont(font);

        gridLayout1->addWidget(Edit_Password_w, 1, 0, 1, 1);

        ButtonEchoMode = new QToolButton(EditEntryDialog);
        ButtonEchoMode->setObjectName(QString::fromUtf8("ButtonEchoMode"));
        sizePolicy.setHeightForWidth(ButtonEchoMode->sizePolicy().hasHeightForWidth());
        ButtonEchoMode->setSizePolicy(sizePolicy);
        ButtonEchoMode->setMinimumSize(QSize(0, 23));
        ButtonEchoMode->setMaximumSize(QSize(16777215, 23));
        ButtonEchoMode->setIconSize(QSize(16, 16));

        gridLayout1->addWidget(ButtonEchoMode, 0, 1, 1, 1);

        Edit_Password = new QLineEdit(EditEntryDialog);
        Edit_Password->setObjectName(QString::fromUtf8("Edit_Password"));
        Edit_Password->setFont(font);

        gridLayout1->addWidget(Edit_Password, 0, 0, 1, 1);


        gridLayout->addLayout(gridLayout1, 5, 1, 2, 1);

        textLabel9 = new QLabel(EditEntryDialog);
        textLabel9->setObjectName(QString::fromUtf8("textLabel9"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLabel9->sizePolicy().hasHeightForWidth());
        textLabel9->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel9, 7, 0, 1, 1);

        textLabel12 = new QLabel(EditEntryDialog);
        textLabel12->setObjectName(QString::fromUtf8("textLabel12"));
        sizePolicy1.setHeightForWidth(textLabel12->sizePolicy().hasHeightForWidth());
        textLabel12->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel12, 10, 0, 1, 1);

        textLabel4 = new QLabel(EditEntryDialog);
        textLabel4->setObjectName(QString::fromUtf8("textLabel4"));
        sizePolicy1.setHeightForWidth(textLabel4->sizePolicy().hasHeightForWidth());
        textLabel4->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel4, 2, 0, 1, 1);

        textLabel5 = new QLabel(EditEntryDialog);
        textLabel5->setObjectName(QString::fromUtf8("textLabel5"));
        sizePolicy1.setHeightForWidth(textLabel5->sizePolicy().hasHeightForWidth());
        textLabel5->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel5, 3, 0, 1, 1);

        textLabel10 = new QLabel(EditEntryDialog);
        textLabel10->setObjectName(QString::fromUtf8("textLabel10"));
        sizePolicy1.setHeightForWidth(textLabel10->sizePolicy().hasHeightForWidth());
        textLabel10->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel10, 8, 0, 1, 1);

        textLabel7 = new QLabel(EditEntryDialog);
        textLabel7->setObjectName(QString::fromUtf8("textLabel7"));
        sizePolicy1.setHeightForWidth(textLabel7->sizePolicy().hasHeightForWidth());
        textLabel7->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel7, 4, 0, 1, 1);

        textLabel3 = new QLabel(EditEntryDialog);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        sizePolicy1.setHeightForWidth(textLabel3->sizePolicy().hasHeightForWidth());
        textLabel3->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel3, 1, 0, 1, 1);

        Edit_Title = new QLineEdit(EditEntryDialog);
        Edit_Title->setObjectName(QString::fromUtf8("Edit_Title"));

        gridLayout->addWidget(Edit_Title, 2, 1, 1, 1);

        Edit_UserName = new QLineEdit(EditEntryDialog);
        Edit_UserName->setObjectName(QString::fromUtf8("Edit_UserName"));

        gridLayout->addWidget(Edit_UserName, 3, 1, 1, 1);

        textLabel8 = new QLabel(EditEntryDialog);
        textLabel8->setObjectName(QString::fromUtf8("textLabel8"));
        sizePolicy1.setHeightForWidth(textLabel8->sizePolicy().hasHeightForWidth());
        textLabel8->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel8, 6, 0, 1, 1);

        textLabel6 = new QLabel(EditEntryDialog);
        textLabel6->setObjectName(QString::fromUtf8("textLabel6"));
        sizePolicy1.setHeightForWidth(textLabel6->sizePolicy().hasHeightForWidth());
        textLabel6->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel6, 5, 0, 1, 1);

        textLabel11 = new QLabel(EditEntryDialog);
        textLabel11->setObjectName(QString::fromUtf8("textLabel11"));
        sizePolicy1.setHeightForWidth(textLabel11->sizePolicy().hasHeightForWidth());
        textLabel11->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(textLabel11, 9, 0, 1, 1);

        Edit_Comment = new QTextEdit(EditEntryDialog);
        Edit_Comment->setObjectName(QString::fromUtf8("Edit_Comment"));
        Edit_Comment->setMinimumSize(QSize(0, 40));
        Edit_Comment->setTabChangesFocus(true);
        Edit_Comment->setAcceptRichText(false);

        gridLayout->addWidget(Edit_Comment, 8, 1, 1, 1);

        line1 = new QFrame(EditEntryDialog);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);
        line1->setFrameShape(QFrame::HLine);

        gridLayout->addWidget(line1, 12, 0, 1, 2);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        Edit_Attachment = new QLineEdit(EditEntryDialog);
        Edit_Attachment->setObjectName(QString::fromUtf8("Edit_Attachment"));
        Edit_Attachment->setEnabled(false);
        sizePolicy.setHeightForWidth(Edit_Attachment->sizePolicy().hasHeightForWidth());
        Edit_Attachment->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(Edit_Attachment);

        ButtonOpenAttachment = new QToolButton(EditEntryDialog);
        ButtonOpenAttachment->setObjectName(QString::fromUtf8("ButtonOpenAttachment"));
        QIcon icon;
        icon.addFile(QString::fromUtf8(""), QSize(), QIcon::Normal, QIcon::Off);
        ButtonOpenAttachment->setIcon(icon);
        ButtonOpenAttachment->setIconSize(QSize(16, 16));

        hboxLayout->addWidget(ButtonOpenAttachment);

        ButtonSaveAttachment = new QToolButton(EditEntryDialog);
        ButtonSaveAttachment->setObjectName(QString::fromUtf8("ButtonSaveAttachment"));
        ButtonSaveAttachment->setIcon(icon);
        ButtonSaveAttachment->setIconSize(QSize(16, 16));

        hboxLayout->addWidget(ButtonSaveAttachment);

        ButtonDeleteAttachment = new QToolButton(EditEntryDialog);
        ButtonDeleteAttachment->setObjectName(QString::fromUtf8("ButtonDeleteAttachment"));
        ButtonDeleteAttachment->setIcon(icon);
        ButtonDeleteAttachment->setIconSize(QSize(16, 16));

        hboxLayout->addWidget(ButtonDeleteAttachment);

        Label_AttachmentSize = new QLabel(EditEntryDialog);
        Label_AttachmentSize->setObjectName(QString::fromUtf8("Label_AttachmentSize"));
        sizePolicy1.setHeightForWidth(Label_AttachmentSize->sizePolicy().hasHeightForWidth());
        Label_AttachmentSize->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(Label_AttachmentSize);


        gridLayout->addLayout(hboxLayout, 10, 1, 1, 1);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        DateTime_Expire = new QDateTimeEdit(EditEntryDialog);
        DateTime_Expire->setObjectName(QString::fromUtf8("DateTime_Expire"));

        hboxLayout1->addWidget(DateTime_Expire);

        ButtonExpirePresets = new QToolButton(EditEntryDialog);
        ButtonExpirePresets->setObjectName(QString::fromUtf8("ButtonExpirePresets"));
        ButtonExpirePresets->setIconSize(QSize(16, 16));
        ButtonExpirePresets->setPopupMode(QToolButton::MenuButtonPopup);

        hboxLayout1->addWidget(ButtonExpirePresets);

        CheckBox_ExpiresNever = new QCheckBox(EditEntryDialog);
        CheckBox_ExpiresNever->setObjectName(QString::fromUtf8("CheckBox_ExpiresNever"));

        hboxLayout1->addWidget(CheckBox_ExpiresNever);


        gridLayout->addLayout(hboxLayout1, 9, 1, 1, 1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        Progress_Quali = new QProgressBar(EditEntryDialog);
        Progress_Quali->setObjectName(QString::fromUtf8("Progress_Quali"));
        Progress_Quali->setMinimumSize(QSize(0, 14));
        Progress_Quali->setMaximumSize(QSize(16777215, 14));
        Progress_Quali->setTextVisible(false);
        Progress_Quali->setOrientation(Qt::Horizontal);

        hboxLayout2->addWidget(Progress_Quali);

        Label_Bits = new QLabel(EditEntryDialog);
        Label_Bits->setObjectName(QString::fromUtf8("Label_Bits"));

        hboxLayout2->addWidget(Label_Bits);


        gridLayout->addLayout(hboxLayout2, 7, 1, 1, 1);

        Edit_URL = new QLineEdit(EditEntryDialog);
        Edit_URL->setObjectName(QString::fromUtf8("Edit_URL"));

        gridLayout->addWidget(Edit_URL, 4, 1, 1, 1);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        Combo_Group = new QComboBox(EditEntryDialog);
        Combo_Group->setObjectName(QString::fromUtf8("Combo_Group"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Combo_Group->sizePolicy().hasHeightForWidth());
        Combo_Group->setSizePolicy(sizePolicy2);

        hboxLayout3->addWidget(Combo_Group);

        spacerItem = new QSpacerItem(100, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout3->addItem(spacerItem);

        textLabel1 = new QLabel(EditEntryDialog);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        textLabel1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        hboxLayout3->addWidget(textLabel1);

        Button_Icons = new QToolButton(EditEntryDialog);
        Button_Icons->setObjectName(QString::fromUtf8("Button_Icons"));
        Button_Icons->setIconSize(QSize(16, 16));

        hboxLayout3->addWidget(Button_Icons);


        gridLayout->addLayout(hboxLayout3, 1, 1, 1, 1);

        buttonBox = new QDialogButtonBox(EditEntryDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 13, 0, 1, 2);

        spacerItem1 = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem1, 0, 0, 1, 2);

        QWidget::setTabOrder(Edit_Title, Edit_UserName);
        QWidget::setTabOrder(Edit_UserName, Edit_URL);
        QWidget::setTabOrder(Edit_URL, Edit_Password);
        QWidget::setTabOrder(Edit_Password, Edit_Password_w);
        QWidget::setTabOrder(Edit_Password_w, ButtonEchoMode);
        QWidget::setTabOrder(ButtonEchoMode, ButtonGenPw);
        QWidget::setTabOrder(ButtonGenPw, Edit_Comment);
        QWidget::setTabOrder(Edit_Comment, DateTime_Expire);
        QWidget::setTabOrder(DateTime_Expire, ButtonExpirePresets);
        QWidget::setTabOrder(ButtonExpirePresets, CheckBox_ExpiresNever);
        QWidget::setTabOrder(CheckBox_ExpiresNever, Edit_Attachment);
        QWidget::setTabOrder(Edit_Attachment, ButtonOpenAttachment);
        QWidget::setTabOrder(ButtonOpenAttachment, ButtonSaveAttachment);
        QWidget::setTabOrder(ButtonSaveAttachment, ButtonDeleteAttachment);
        QWidget::setTabOrder(ButtonDeleteAttachment, Combo_Group);
        QWidget::setTabOrder(Combo_Group, Button_Icons);
        QWidget::setTabOrder(Button_Icons, buttonBox);

        retranslateUi(EditEntryDialog);

        QMetaObject::connectSlotsByName(EditEntryDialog);
    } // setupUi

    void retranslateUi(QDialog *EditEntryDialog)
    {
        EditEntryDialog->setWindowTitle(QApplication::translate("EditEntryDialog", "Edit Entry", 0, QApplication::UnicodeUTF8));
        ButtonGenPw->setText(QApplication::translate("EditEntryDialog", "Ge&n.", 0, QApplication::UnicodeUTF8));
        textLabel9->setText(QApplication::translate("EditEntryDialog", "Quality:", 0, QApplication::UnicodeUTF8));
        textLabel12->setText(QApplication::translate("EditEntryDialog", "Attachment:", 0, QApplication::UnicodeUTF8));
        textLabel4->setText(QApplication::translate("EditEntryDialog", "Title:", 0, QApplication::UnicodeUTF8));
        textLabel5->setText(QApplication::translate("EditEntryDialog", "Username:", 0, QApplication::UnicodeUTF8));
        textLabel10->setText(QApplication::translate("EditEntryDialog", "Comment:", 0, QApplication::UnicodeUTF8));
        textLabel7->setText(QApplication::translate("EditEntryDialog", "URL:", 0, QApplication::UnicodeUTF8));
        textLabel3->setText(QApplication::translate("EditEntryDialog", "Group:", 0, QApplication::UnicodeUTF8));
        textLabel8->setText(QApplication::translate("EditEntryDialog", "Repeat:", 0, QApplication::UnicodeUTF8));
        textLabel6->setText(QApplication::translate("EditEntryDialog", "Password:", 0, QApplication::UnicodeUTF8));
        textLabel11->setText(QApplication::translate("EditEntryDialog", "Expires:", 0, QApplication::UnicodeUTF8));
        ButtonOpenAttachment->setText(QString());
        ButtonSaveAttachment->setText(QString());
        ButtonDeleteAttachment->setText(QString());
        Label_AttachmentSize->setText(QApplication::translate("EditEntryDialog", "%1", 0, QApplication::UnicodeUTF8));
        ButtonExpirePresets->setText(QString());
        CheckBox_ExpiresNever->setText(QApplication::translate("EditEntryDialog", "Never", 0, QApplication::UnicodeUTF8));
        Label_Bits->setText(QApplication::translate("EditEntryDialog", "%1 Bit", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("EditEntryDialog", "Icon:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EditEntryDialog: public Ui_EditEntryDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITENTRYDLG_H
