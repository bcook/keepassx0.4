/********************************************************************************
** Form generated from reading UI file 'PasswordGenDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PASSWORDGENDLG_H
#define UI_PASSWORDGENDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QTabWidget>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GenPwDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QTabWidget *tabCategory;
    QWidget *tabRandom;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem1;
    QGridLayout *gridLayout;
    QCheckBox *checkBox2;
    QCheckBox *checkBox7;
    QCheckBox *checkBox3;
    QCheckBox *checkBox5;
    QCheckBox *checkBox1;
    QCheckBox *checkBox6;
    QCheckBox *checkBox4;
    QCheckBox *Check_ExcludeLookAlike;
    QCheckBox *Check_EveryGroup;
    QWidget *tabPronounceable;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout_2;
    QCheckBox *checkBoxPL;
    QCheckBox *checkBoxPU;
    QCheckBox *checkBoxPN;
    QCheckBox *checkBoxPS;
    QSpacerItem *verticalSpacer;
    QWidget *tabCustom;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer_3;
    QLabel *label;
    QLineEdit *Edit_chars;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *spacerItem2;
    QGroupBox *groupBox1;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout1;
    QLabel *textLabel1;
    QSpinBox *Spin_Num;
    QSpacerItem *spacerItem3;
    QLabel *textLabel5;
    QProgressBar *Progress_Quali;
    QHBoxLayout *hboxLayout2;
    QCheckBox *Check_CollectEntropy;
    QSpacerItem *spacerItem4;
    QCheckBox *Check_CollectOncePerSession;
    QSpacerItem *spacerItem5;
    QHBoxLayout *hboxLayout3;
    QLabel *textLabel4;
    QLineEdit *Edit_dest;
    QToolButton *ButtonChangeEchoMode;
    QPushButton *ButtonGenerate;
    QFrame *line3;
    QDialogButtonBox *DialogButtons;

    void setupUi(QDialog *GenPwDlg)
    {
        if (GenPwDlg->objectName().isEmpty())
            GenPwDlg->setObjectName(QString::fromUtf8("GenPwDlg"));
        GenPwDlg->resize(460, 432);
        vboxLayout = new QVBoxLayout(GenPwDlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(442, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        tabCategory = new QTabWidget(GenPwDlg);
        tabCategory->setObjectName(QString::fromUtf8("tabCategory"));
        tabRandom = new QWidget();
        tabRandom->setObjectName(QString::fromUtf8("tabRandom"));
        verticalLayout = new QVBoxLayout(tabRandom);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_2 = new QLabel(tabRandom);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout->addWidget(label_2);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacerItem1 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        checkBox2 = new QCheckBox(tabRandom);
        checkBox2->setObjectName(QString::fromUtf8("checkBox2"));
        checkBox2->setChecked(true);

        gridLayout->addWidget(checkBox2, 1, 0, 1, 1);

        checkBox7 = new QCheckBox(tabRandom);
        checkBox7->setObjectName(QString::fromUtf8("checkBox7"));

        gridLayout->addWidget(checkBox7, 2, 1, 1, 1);

        checkBox3 = new QCheckBox(tabRandom);
        checkBox3->setObjectName(QString::fromUtf8("checkBox3"));
        checkBox3->setChecked(true);

        gridLayout->addWidget(checkBox3, 2, 0, 1, 1);

        checkBox5 = new QCheckBox(tabRandom);
        checkBox5->setObjectName(QString::fromUtf8("checkBox5"));

        gridLayout->addWidget(checkBox5, 0, 1, 1, 1);

        checkBox1 = new QCheckBox(tabRandom);
        checkBox1->setObjectName(QString::fromUtf8("checkBox1"));
        checkBox1->setChecked(true);

        gridLayout->addWidget(checkBox1, 0, 0, 1, 1);

        checkBox6 = new QCheckBox(tabRandom);
        checkBox6->setObjectName(QString::fromUtf8("checkBox6"));

        gridLayout->addWidget(checkBox6, 1, 1, 1, 1);

        checkBox4 = new QCheckBox(tabRandom);
        checkBox4->setObjectName(QString::fromUtf8("checkBox4"));

        gridLayout->addWidget(checkBox4, 0, 2, 1, 1);


        hboxLayout->addLayout(gridLayout);


        verticalLayout->addLayout(hboxLayout);

        Check_ExcludeLookAlike = new QCheckBox(tabRandom);
        Check_ExcludeLookAlike->setObjectName(QString::fromUtf8("Check_ExcludeLookAlike"));

        verticalLayout->addWidget(Check_ExcludeLookAlike);

        Check_EveryGroup = new QCheckBox(tabRandom);
        Check_EveryGroup->setObjectName(QString::fromUtf8("Check_EveryGroup"));

        verticalLayout->addWidget(Check_EveryGroup);

        tabCategory->addTab(tabRandom, QString());
        tabPronounceable = new QWidget();
        tabPronounceable->setObjectName(QString::fromUtf8("tabPronounceable"));
        verticalLayout_2 = new QVBoxLayout(tabPronounceable);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_2 = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setVerticalSpacing(12);
        checkBoxPL = new QCheckBox(tabPronounceable);
        checkBoxPL->setObjectName(QString::fromUtf8("checkBoxPL"));

        gridLayout_2->addWidget(checkBoxPL, 0, 0, 1, 1);

        checkBoxPU = new QCheckBox(tabPronounceable);
        checkBoxPU->setObjectName(QString::fromUtf8("checkBoxPU"));

        gridLayout_2->addWidget(checkBoxPU, 1, 0, 1, 1);

        checkBoxPN = new QCheckBox(tabPronounceable);
        checkBoxPN->setObjectName(QString::fromUtf8("checkBoxPN"));

        gridLayout_2->addWidget(checkBoxPN, 2, 0, 1, 1);

        checkBoxPS = new QCheckBox(tabPronounceable);
        checkBoxPS->setObjectName(QString::fromUtf8("checkBoxPS"));

        gridLayout_2->addWidget(checkBoxPS, 3, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout_2);

        verticalSpacer = new QSpacerItem(20, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        tabCategory->addTab(tabPronounceable, QString());
        tabCustom = new QWidget();
        tabCustom->setObjectName(QString::fromUtf8("tabCustom"));
        verticalLayout_3 = new QVBoxLayout(tabCustom);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer_3 = new QSpacerItem(20, 51, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);

        label = new QLabel(tabCustom);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_3->addWidget(label);

        Edit_chars = new QLineEdit(tabCustom);
        Edit_chars->setObjectName(QString::fromUtf8("Edit_chars"));
        QFont font;
        font.setFamily(QString::fromUtf8("Serif"));
        Edit_chars->setFont(font);
        Edit_chars->setMaxLength(255);

        verticalLayout_3->addWidget(Edit_chars);

        verticalSpacer_4 = new QSpacerItem(20, 51, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_4);

        tabCategory->addTab(tabCustom, QString());

        vboxLayout->addWidget(tabCategory);

        spacerItem2 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem2);

        groupBox1 = new QGroupBox(GenPwDlg);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));
        vboxLayout1 = new QVBoxLayout(groupBox1);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        textLabel1 = new QLabel(groupBox1);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        hboxLayout1->addWidget(textLabel1);

        Spin_Num = new QSpinBox(groupBox1);
        Spin_Num->setObjectName(QString::fromUtf8("Spin_Num"));
        Spin_Num->setMinimum(1);
        Spin_Num->setMaximum(10000);
        Spin_Num->setValue(20);

        hboxLayout1->addWidget(Spin_Num);

        spacerItem3 = new QSpacerItem(10, 1, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem3);

        textLabel5 = new QLabel(groupBox1);
        textLabel5->setObjectName(QString::fromUtf8("textLabel5"));

        hboxLayout1->addWidget(textLabel5);

        Progress_Quali = new QProgressBar(groupBox1);
        Progress_Quali->setObjectName(QString::fromUtf8("Progress_Quali"));
        Progress_Quali->setMaximum(128);
        Progress_Quali->setValue(0);
        Progress_Quali->setOrientation(Qt::Horizontal);

        hboxLayout1->addWidget(Progress_Quali);


        vboxLayout1->addLayout(hboxLayout1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        Check_CollectEntropy = new QCheckBox(groupBox1);
        Check_CollectEntropy->setObjectName(QString::fromUtf8("Check_CollectEntropy"));
        Check_CollectEntropy->setChecked(true);

        hboxLayout2->addWidget(Check_CollectEntropy);

        spacerItem4 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem4);

        Check_CollectOncePerSession = new QCheckBox(groupBox1);
        Check_CollectOncePerSession->setObjectName(QString::fromUtf8("Check_CollectOncePerSession"));

        hboxLayout2->addWidget(Check_CollectOncePerSession);

        spacerItem5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout2->addItem(spacerItem5);


        vboxLayout1->addLayout(hboxLayout2);


        vboxLayout->addWidget(groupBox1);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        textLabel4 = new QLabel(GenPwDlg);
        textLabel4->setObjectName(QString::fromUtf8("textLabel4"));

        hboxLayout3->addWidget(textLabel4);

        Edit_dest = new QLineEdit(GenPwDlg);
        Edit_dest->setObjectName(QString::fromUtf8("Edit_dest"));
        Edit_dest->setFont(font);

        hboxLayout3->addWidget(Edit_dest);

        ButtonChangeEchoMode = new QToolButton(GenPwDlg);
        ButtonChangeEchoMode->setObjectName(QString::fromUtf8("ButtonChangeEchoMode"));

        hboxLayout3->addWidget(ButtonChangeEchoMode);

        ButtonGenerate = new QPushButton(GenPwDlg);
        ButtonGenerate->setObjectName(QString::fromUtf8("ButtonGenerate"));

        hboxLayout3->addWidget(ButtonGenerate);


        vboxLayout->addLayout(hboxLayout3);

        line3 = new QFrame(GenPwDlg);
        line3->setObjectName(QString::fromUtf8("line3"));
        line3->setFrameShape(QFrame::HLine);
        line3->setFrameShadow(QFrame::Sunken);
        line3->setFrameShape(QFrame::HLine);

        vboxLayout->addWidget(line3);

        DialogButtons = new QDialogButtonBox(GenPwDlg);
        DialogButtons->setObjectName(QString::fromUtf8("DialogButtons"));
        DialogButtons->setOrientation(Qt::Horizontal);
        DialogButtons->setStandardButtons(QDialogButtonBox::NoButton);

        vboxLayout->addWidget(DialogButtons);

        QWidget::setTabOrder(tabCategory, checkBox1);
        QWidget::setTabOrder(checkBox1, checkBox2);
        QWidget::setTabOrder(checkBox2, checkBox3);
        QWidget::setTabOrder(checkBox3, checkBox5);
        QWidget::setTabOrder(checkBox5, checkBox6);
        QWidget::setTabOrder(checkBox6, checkBox7);
        QWidget::setTabOrder(checkBox7, checkBox4);
        QWidget::setTabOrder(checkBox4, Check_ExcludeLookAlike);
        QWidget::setTabOrder(Check_ExcludeLookAlike, Check_EveryGroup);
        QWidget::setTabOrder(Check_EveryGroup, Spin_Num);
        QWidget::setTabOrder(Spin_Num, Check_CollectEntropy);
        QWidget::setTabOrder(Check_CollectEntropy, Check_CollectOncePerSession);
        QWidget::setTabOrder(Check_CollectOncePerSession, Edit_dest);
        QWidget::setTabOrder(Edit_dest, ButtonChangeEchoMode);
        QWidget::setTabOrder(ButtonChangeEchoMode, ButtonGenerate);
        QWidget::setTabOrder(ButtonGenerate, checkBoxPL);
        QWidget::setTabOrder(checkBoxPL, checkBoxPU);
        QWidget::setTabOrder(checkBoxPU, checkBoxPN);
        QWidget::setTabOrder(checkBoxPN, checkBoxPS);
        QWidget::setTabOrder(checkBoxPS, DialogButtons);

        retranslateUi(GenPwDlg);

        tabCategory->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(GenPwDlg);
    } // setupUi

    void retranslateUi(QDialog *GenPwDlg)
    {
        GenPwDlg->setWindowTitle(QApplication::translate("GenPwDlg", "Password Generator", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("GenPwDlg", "Use following character groups:", 0, QApplication::UnicodeUTF8));
        checkBox2->setText(QApplication::translate("GenPwDlg", "&Lower Letters", 0, QApplication::UnicodeUTF8));
        checkBox7->setText(QApplication::translate("GenPwDlg", "&Underline", 0, QApplication::UnicodeUTF8));
        checkBox3->setText(QApplication::translate("GenPwDlg", "&Numbers", 0, QApplication::UnicodeUTF8));
        checkBox5->setText(QApplication::translate("GenPwDlg", "&White Spaces", 0, QApplication::UnicodeUTF8));
        checkBox1->setText(QApplication::translate("GenPwDlg", "&Upper Letters", 0, QApplication::UnicodeUTF8));
        checkBox6->setText(QApplication::translate("GenPwDlg", "&Minus", 0, QApplication::UnicodeUTF8));
        checkBox4->setText(QApplication::translate("GenPwDlg", "&Special Characters", 0, QApplication::UnicodeUTF8));
        Check_ExcludeLookAlike->setText(QApplication::translate("GenPwDlg", "Exclude look-alike characters", 0, QApplication::UnicodeUTF8));
        Check_EveryGroup->setText(QApplication::translate("GenPwDlg", "Ensure that password contains characters from every group", 0, QApplication::UnicodeUTF8));
        tabCategory->setTabText(tabCategory->indexOf(tabRandom), QApplication::translate("GenPwDlg", "Random", 0, QApplication::UnicodeUTF8));
        checkBoxPL->setText(QApplication::translate("GenPwDlg", "Lower Letters", 0, QApplication::UnicodeUTF8));
        checkBoxPU->setText(QApplication::translate("GenPwDlg", "Upper Letters", 0, QApplication::UnicodeUTF8));
        checkBoxPN->setText(QApplication::translate("GenPwDlg", "Numbers", 0, QApplication::UnicodeUTF8));
        checkBoxPS->setText(QApplication::translate("GenPwDlg", "Special Characters", 0, QApplication::UnicodeUTF8));
        tabCategory->setTabText(tabCategory->indexOf(tabPronounceable), QApplication::translate("GenPwDlg", "Pronounceable", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("GenPwDlg", "Use the following characters:", 0, QApplication::UnicodeUTF8));
        tabCategory->setTabText(tabCategory->indexOf(tabCustom), QApplication::translate("GenPwDlg", "Custom", 0, QApplication::UnicodeUTF8));
        groupBox1->setTitle(QApplication::translate("GenPwDlg", "Options", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("GenPwDlg", "Length:", 0, QApplication::UnicodeUTF8));
        textLabel5->setText(QApplication::translate("GenPwDlg", "Quality:", 0, QApplication::UnicodeUTF8));
        Check_CollectEntropy->setText(QApplication::translate("GenPwDlg", "Enable entropy collection", 0, QApplication::UnicodeUTF8));
        Check_CollectOncePerSession->setText(QApplication::translate("GenPwDlg", "Collect only once per session", 0, QApplication::UnicodeUTF8));
        textLabel4->setText(QApplication::translate("GenPwDlg", "New Password:", 0, QApplication::UnicodeUTF8));
        ButtonChangeEchoMode->setText(QString());
        ButtonGenerate->setText(QApplication::translate("GenPwDlg", "Generate", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GenPwDlg: public Ui_GenPwDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PASSWORDGENDLG_H
