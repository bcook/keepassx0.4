/********************************************************************************
** Form generated from reading UI file 'DatabaseSettingsDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATABASESETTINGSDLG_H
#define UI_DATABASESETTINGSDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DatabaseSettingsDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QGroupBox *groupBox1;
    QGridLayout *gridLayout;
    QLabel *textLabel2;
    QComboBox *ComboAlgo;
    QLabel *textLabel3;
    QHBoxLayout *hboxLayout;
    QLineEdit *EditRounds;
    QPushButton *ButtonBench;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *DatabaseSettingsDlg)
    {
        if (DatabaseSettingsDlg->objectName().isEmpty())
            DatabaseSettingsDlg->setObjectName(QString::fromUtf8("DatabaseSettingsDlg"));
        DatabaseSettingsDlg->resize(440, 213);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(DatabaseSettingsDlg->sizePolicy().hasHeightForWidth());
        DatabaseSettingsDlg->setSizePolicy(sizePolicy);
        DatabaseSettingsDlg->setMinimumSize(QSize(440, 0));
        vboxLayout = new QVBoxLayout(DatabaseSettingsDlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        groupBox1 = new QGroupBox(DatabaseSettingsDlg);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));
        gridLayout = new QGridLayout(groupBox1);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(15);
        textLabel2 = new QLabel(groupBox1);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));

        gridLayout->addWidget(textLabel2, 0, 0, 1, 1);

        ComboAlgo = new QComboBox(groupBox1);
        ComboAlgo->setObjectName(QString::fromUtf8("ComboAlgo"));

        gridLayout->addWidget(ComboAlgo, 0, 1, 1, 1);

        textLabel3 = new QLabel(groupBox1);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));

        gridLayout->addWidget(textLabel3, 1, 0, 1, 1);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        EditRounds = new QLineEdit(groupBox1);
        EditRounds->setObjectName(QString::fromUtf8("EditRounds"));

        hboxLayout->addWidget(EditRounds);

        ButtonBench = new QPushButton(groupBox1);
        ButtonBench->setObjectName(QString::fromUtf8("ButtonBench"));
        sizePolicy.setHeightForWidth(ButtonBench->sizePolicy().hasHeightForWidth());
        ButtonBench->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(ButtonBench);


        gridLayout->addLayout(hboxLayout, 1, 1, 1, 1);


        vboxLayout->addWidget(groupBox1);

        ButtonBox = new QDialogButtonBox(DatabaseSettingsDlg);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(ButtonBox);

        QWidget::setTabOrder(ComboAlgo, EditRounds);
        QWidget::setTabOrder(EditRounds, ButtonBench);
        QWidget::setTabOrder(ButtonBench, ButtonBox);

        retranslateUi(DatabaseSettingsDlg);

        QMetaObject::connectSlotsByName(DatabaseSettingsDlg);
    } // setupUi

    void retranslateUi(QDialog *DatabaseSettingsDlg)
    {
        DatabaseSettingsDlg->setWindowTitle(QApplication::translate("DatabaseSettingsDlg", "Database Settings", 0, QApplication::UnicodeUTF8));
        groupBox1->setTitle(QApplication::translate("DatabaseSettingsDlg", "Encryption", 0, QApplication::UnicodeUTF8));
        textLabel2->setText(QApplication::translate("DatabaseSettingsDlg", "Algorithm:", 0, QApplication::UnicodeUTF8));
        textLabel3->setText(QApplication::translate("DatabaseSettingsDlg", "Encryption Rounds:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        ButtonBench->setToolTip(QApplication::translate("DatabaseSettingsDlg", "Calculate rounds for a 1-second delay on this computer", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        ButtonBench->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class DatabaseSettingsDlg: public Ui_DatabaseSettingsDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATABASESETTINGSDLG_H
