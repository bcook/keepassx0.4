/********************************************************************************
** Form generated from reading UI file 'SearchDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCHDLG_H
#define UI_SEARCHDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Search_Dlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QGridLayout *gridLayout;
    QSpacerItem *spacerItem1;
    QGroupBox *groupBox1;
    QGridLayout *gridLayout1;
    QCheckBox *checkBox_Password;
    QCheckBox *checkBox_Attachment;
    QCheckBox *checkBox_URL;
    QCheckBox *checkBox_Comment;
    QCheckBox *checkBox_Username;
    QCheckBox *checkBox_Title;
    QVBoxLayout *vboxLayout1;
    QCheckBox *checkBox_Cs;
    QCheckBox *checkBox_regExp;
    QCheckBox *checkBox_Recursive;
    QLineEdit *Edit_Search;
    QLabel *textLabel1;
    QSpacerItem *spacerItem2;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *Search_Dlg)
    {
        if (Search_Dlg->objectName().isEmpty())
            Search_Dlg->setObjectName(QString::fromUtf8("Search_Dlg"));
        Search_Dlg->resize(458, 306);
        Search_Dlg->setMinimumSize(QSize(390, 0));
        vboxLayout = new QVBoxLayout(Search_Dlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(4);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem1, 1, 0, 1, 1);

        groupBox1 = new QGroupBox(Search_Dlg);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));
        gridLayout1 = new QGridLayout(groupBox1);
        gridLayout1->setSpacing(6);
        gridLayout1->setContentsMargins(11, 11, 11, 11);
        gridLayout1->setObjectName(QString::fromUtf8("gridLayout1"));
        checkBox_Password = new QCheckBox(groupBox1);
        checkBox_Password->setObjectName(QString::fromUtf8("checkBox_Password"));

        gridLayout1->addWidget(checkBox_Password, 0, 1, 1, 1);

        checkBox_Attachment = new QCheckBox(groupBox1);
        checkBox_Attachment->setObjectName(QString::fromUtf8("checkBox_Attachment"));

        gridLayout1->addWidget(checkBox_Attachment, 1, 2, 1, 1);

        checkBox_URL = new QCheckBox(groupBox1);
        checkBox_URL->setObjectName(QString::fromUtf8("checkBox_URL"));

        gridLayout1->addWidget(checkBox_URL, 0, 2, 1, 1);

        checkBox_Comment = new QCheckBox(groupBox1);
        checkBox_Comment->setObjectName(QString::fromUtf8("checkBox_Comment"));

        gridLayout1->addWidget(checkBox_Comment, 1, 1, 1, 1);

        checkBox_Username = new QCheckBox(groupBox1);
        checkBox_Username->setObjectName(QString::fromUtf8("checkBox_Username"));

        gridLayout1->addWidget(checkBox_Username, 1, 0, 1, 1);

        checkBox_Title = new QCheckBox(groupBox1);
        checkBox_Title->setObjectName(QString::fromUtf8("checkBox_Title"));

        gridLayout1->addWidget(checkBox_Title, 0, 0, 1, 1);


        gridLayout->addWidget(groupBox1, 2, 0, 1, 2);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setSpacing(0);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        checkBox_Cs = new QCheckBox(Search_Dlg);
        checkBox_Cs->setObjectName(QString::fromUtf8("checkBox_Cs"));

        vboxLayout1->addWidget(checkBox_Cs);

        checkBox_regExp = new QCheckBox(Search_Dlg);
        checkBox_regExp->setObjectName(QString::fromUtf8("checkBox_regExp"));

        vboxLayout1->addWidget(checkBox_regExp);

        checkBox_Recursive = new QCheckBox(Search_Dlg);
        checkBox_Recursive->setObjectName(QString::fromUtf8("checkBox_Recursive"));

        vboxLayout1->addWidget(checkBox_Recursive);


        gridLayout->addLayout(vboxLayout1, 1, 1, 1, 1);

        Edit_Search = new QLineEdit(Search_Dlg);
        Edit_Search->setObjectName(QString::fromUtf8("Edit_Search"));

        gridLayout->addWidget(Edit_Search, 0, 1, 1, 1);

        textLabel1 = new QLabel(Search_Dlg);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));

        gridLayout->addWidget(textLabel1, 0, 0, 1, 1);


        vboxLayout->addLayout(gridLayout);

        spacerItem2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem2);

        ButtonBox = new QDialogButtonBox(Search_Dlg);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Close);

        vboxLayout->addWidget(ButtonBox);

        QWidget::setTabOrder(Edit_Search, checkBox_Cs);
        QWidget::setTabOrder(checkBox_Cs, checkBox_regExp);
        QWidget::setTabOrder(checkBox_regExp, checkBox_Recursive);
        QWidget::setTabOrder(checkBox_Recursive, checkBox_Title);
        QWidget::setTabOrder(checkBox_Title, checkBox_Username);
        QWidget::setTabOrder(checkBox_Username, checkBox_Password);
        QWidget::setTabOrder(checkBox_Password, checkBox_Comment);
        QWidget::setTabOrder(checkBox_Comment, checkBox_URL);
        QWidget::setTabOrder(checkBox_URL, checkBox_Attachment);
        QWidget::setTabOrder(checkBox_Attachment, ButtonBox);

        retranslateUi(Search_Dlg);

        QMetaObject::connectSlotsByName(Search_Dlg);
    } // setupUi

    void retranslateUi(QDialog *Search_Dlg)
    {
        Search_Dlg->setWindowTitle(QApplication::translate("Search_Dlg", "Search...", 0, QApplication::UnicodeUTF8));
        groupBox1->setTitle(QApplication::translate("Search_Dlg", "Include:", 0, QApplication::UnicodeUTF8));
        checkBox_Password->setText(QApplication::translate("Search_Dlg", "Pass&words", 0, QApplication::UnicodeUTF8));
        checkBox_Attachment->setText(QApplication::translate("Search_Dlg", "A&nhang", 0, QApplication::UnicodeUTF8));
        checkBox_URL->setText(QApplication::translate("Search_Dlg", "U&RLs", 0, QApplication::UnicodeUTF8));
        checkBox_Comment->setText(QApplication::translate("Search_Dlg", "C&omments", 0, QApplication::UnicodeUTF8));
        checkBox_Username->setText(QApplication::translate("Search_Dlg", "&Usernames", 0, QApplication::UnicodeUTF8));
        checkBox_Title->setText(QApplication::translate("Search_Dlg", "&Titles", 0, QApplication::UnicodeUTF8));
        checkBox_Cs->setText(QApplication::translate("Search_Dlg", "&Case Sensitive", 0, QApplication::UnicodeUTF8));
        checkBox_regExp->setText(QApplication::translate("Search_Dlg", "Regular E&xpression", 0, QApplication::UnicodeUTF8));
        checkBox_Recursive->setText(QApplication::translate("Search_Dlg", "Include Subgroups (recursive)", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("Search_Dlg", "Search For:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Search_Dlg: public Ui_Search_Dlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCHDLG_H
