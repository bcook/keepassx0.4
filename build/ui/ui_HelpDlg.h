/********************************************************************************
** Form generated from reading UI file 'HelpDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HELPDLG_H
#define UI_HELPDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_HelpDlg
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *buttonPrevious;
    QPushButton *buttonNext;
    QPushButton *buttonFirst;
    QSpacerItem *horizontalSpacer;
    QTextBrowser *textBrowser;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *HelpDlg)
    {
        if (HelpDlg->objectName().isEmpty())
            HelpDlg->setObjectName(QString::fromUtf8("HelpDlg"));
        HelpDlg->resize(400, 300);
        verticalLayout = new QVBoxLayout(HelpDlg);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        buttonPrevious = new QPushButton(HelpDlg);
        buttonPrevious->setObjectName(QString::fromUtf8("buttonPrevious"));

        horizontalLayout->addWidget(buttonPrevious);

        buttonNext = new QPushButton(HelpDlg);
        buttonNext->setObjectName(QString::fromUtf8("buttonNext"));

        horizontalLayout->addWidget(buttonNext);

        buttonFirst = new QPushButton(HelpDlg);
        buttonFirst->setObjectName(QString::fromUtf8("buttonFirst"));

        horizontalLayout->addWidget(buttonFirst);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        verticalLayout->addLayout(horizontalLayout);

        textBrowser = new QTextBrowser(HelpDlg);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setOpenExternalLinks(true);

        verticalLayout->addWidget(textBrowser);

        buttonBox = new QDialogButtonBox(HelpDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout->addWidget(buttonBox);

        QWidget::setTabOrder(textBrowser, buttonBox);

        retranslateUi(HelpDlg);
        QObject::connect(buttonBox, SIGNAL(accepted()), HelpDlg, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), HelpDlg, SLOT(reject()));
        QObject::connect(buttonPrevious, SIGNAL(clicked()), textBrowser, SLOT(backward()));
        QObject::connect(buttonNext, SIGNAL(clicked()), textBrowser, SLOT(forward()));
        QObject::connect(buttonFirst, SIGNAL(clicked()), textBrowser, SLOT(home()));
        QObject::connect(textBrowser, SIGNAL(backwardAvailable(bool)), buttonPrevious, SLOT(setEnabled(bool)));
        QObject::connect(textBrowser, SIGNAL(forwardAvailable(bool)), buttonNext, SLOT(setEnabled(bool)));

        QMetaObject::connectSlotsByName(HelpDlg);
    } // setupUi

    void retranslateUi(QDialog *HelpDlg)
    {
        HelpDlg->setWindowTitle(QApplication::translate("HelpDlg", "Help Contents", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        buttonPrevious->setToolTip(QApplication::translate("HelpDlg", "Previous Page", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        buttonPrevious->setText(QApplication::translate("HelpDlg", "Previous Page", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        buttonNext->setToolTip(QApplication::translate("HelpDlg", "Next Page", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        buttonNext->setText(QApplication::translate("HelpDlg", "Next Page", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        buttonFirst->setToolTip(QApplication::translate("HelpDlg", "First Page", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        buttonFirst->setText(QApplication::translate("HelpDlg", "First Page", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class HelpDlg: public Ui_HelpDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HELPDLG_H
