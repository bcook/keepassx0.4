/********************************************************************************
** Form generated from reading UI file 'PasswordDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PASSWORDDLG_H
#define UI_PASSWORDDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PasswordDlg
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QSpacerItem *spacerItem1;
    QHBoxLayout *hboxLayout1;
    QLabel *Label_Bookmark;
    QToolButton *Button_Bookmarks;
    QLabel *textLabel1;
    QGroupBox *groupframe;
    QVBoxLayout *vboxLayout1;
    QStackedWidget *stackedWidget;
    QWidget *pw_entry;
    QGridLayout *gridLayout;
    QCheckBox *Check_Password;
    QHBoxLayout *hboxLayout2;
    QLineEdit *Edit_Password;
    QToolButton *ButtonChangeEchoMode;
    QCheckBox *Check_KeyFile;
    QHBoxLayout *hboxLayout3;
    QComboBox *Combo_KeyFile;
    QPushButton *Button_Browse;
    QPushButton *Button_GenKeyFile;
    QSpacerItem *spacerItem2;
    QWidget *pw_repeat;
    QVBoxLayout *vboxLayout2;
    QLabel *label;
    QLineEdit *Edit_PwRepeat;
    QHBoxLayout *hboxLayout4;
    QPushButton *Button_Back;
    QSpacerItem *spacerItem3;
    QLabel *Label_Unequal;
    QSpacerItem *spacerItem4;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *PasswordDlg)
    {
        if (PasswordDlg->objectName().isEmpty())
            PasswordDlg->setObjectName(QString::fromUtf8("PasswordDlg"));
        PasswordDlg->resize(578, 273);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(PasswordDlg->sizePolicy().hasHeightForWidth());
        PasswordDlg->setSizePolicy(sizePolicy);
        PasswordDlg->setMinimumSize(QSize(578, 0));
        PasswordDlg->setSizeGripEnabled(false);
        PasswordDlg->setModal(true);
        vboxLayout = new QVBoxLayout(PasswordDlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        vboxLayout->setContentsMargins(-1, -1, -1, 9);
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        hboxLayout->addItem(spacerItem);

        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem1);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        Label_Bookmark = new QLabel(PasswordDlg);
        Label_Bookmark->setObjectName(QString::fromUtf8("Label_Bookmark"));

        hboxLayout1->addWidget(Label_Bookmark);

        Button_Bookmarks = new QToolButton(PasswordDlg);
        Button_Bookmarks->setObjectName(QString::fromUtf8("Button_Bookmarks"));
        Button_Bookmarks->setPopupMode(QToolButton::InstantPopup);
        Button_Bookmarks->setArrowType(Qt::NoArrow);

        hboxLayout1->addWidget(Button_Bookmarks);


        hboxLayout->addLayout(hboxLayout1);


        vboxLayout->addLayout(hboxLayout);

        textLabel1 = new QLabel(PasswordDlg);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLabel1->sizePolicy().hasHeightForWidth());
        textLabel1->setSizePolicy(sizePolicy1);

        vboxLayout->addWidget(textLabel1);

        groupframe = new QGroupBox(PasswordDlg);
        groupframe->setObjectName(QString::fromUtf8("groupframe"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupframe->sizePolicy().hasHeightForWidth());
        groupframe->setSizePolicy(sizePolicy2);
        vboxLayout1 = new QVBoxLayout(groupframe);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        stackedWidget = new QStackedWidget(groupframe);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        pw_entry = new QWidget();
        pw_entry->setObjectName(QString::fromUtf8("pw_entry"));
        pw_entry->setGeometry(QRect(0, 0, 556, 116));
        gridLayout = new QGridLayout(pw_entry);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Check_Password = new QCheckBox(pw_entry);
        Check_Password->setObjectName(QString::fromUtf8("Check_Password"));
        Check_Password->setChecked(true);

        gridLayout->addWidget(Check_Password, 0, 0, 1, 1);

        hboxLayout2 = new QHBoxLayout();
        hboxLayout2->setSpacing(6);
        hboxLayout2->setObjectName(QString::fromUtf8("hboxLayout2"));
        Edit_Password = new QLineEdit(pw_entry);
        Edit_Password->setObjectName(QString::fromUtf8("Edit_Password"));
        QFont font;
        font.setFamily(QString::fromUtf8("Serif"));
        Edit_Password->setFont(font);

        hboxLayout2->addWidget(Edit_Password);

        ButtonChangeEchoMode = new QToolButton(pw_entry);
        ButtonChangeEchoMode->setObjectName(QString::fromUtf8("ButtonChangeEchoMode"));

        hboxLayout2->addWidget(ButtonChangeEchoMode);


        gridLayout->addLayout(hboxLayout2, 0, 1, 1, 2);

        Check_KeyFile = new QCheckBox(pw_entry);
        Check_KeyFile->setObjectName(QString::fromUtf8("Check_KeyFile"));

        gridLayout->addWidget(Check_KeyFile, 1, 0, 1, 1);

        hboxLayout3 = new QHBoxLayout();
        hboxLayout3->setSpacing(6);
        hboxLayout3->setObjectName(QString::fromUtf8("hboxLayout3"));
        Combo_KeyFile = new QComboBox(pw_entry);
        Combo_KeyFile->setObjectName(QString::fromUtf8("Combo_KeyFile"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(Combo_KeyFile->sizePolicy().hasHeightForWidth());
        Combo_KeyFile->setSizePolicy(sizePolicy3);
        Combo_KeyFile->setEditable(true);

        hboxLayout3->addWidget(Combo_KeyFile);

        Button_Browse = new QPushButton(pw_entry);
        Button_Browse->setObjectName(QString::fromUtf8("Button_Browse"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(Button_Browse->sizePolicy().hasHeightForWidth());
        Button_Browse->setSizePolicy(sizePolicy4);

        hboxLayout3->addWidget(Button_Browse);


        gridLayout->addLayout(hboxLayout3, 1, 1, 1, 2);

        Button_GenKeyFile = new QPushButton(pw_entry);
        Button_GenKeyFile->setObjectName(QString::fromUtf8("Button_GenKeyFile"));

        gridLayout->addWidget(Button_GenKeyFile, 2, 1, 1, 1);

        spacerItem2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem2, 2, 2, 1, 1);

        stackedWidget->addWidget(pw_entry);
        pw_repeat = new QWidget();
        pw_repeat->setObjectName(QString::fromUtf8("pw_repeat"));
        pw_repeat->setGeometry(QRect(0, 0, 556, 116));
        vboxLayout2 = new QVBoxLayout(pw_repeat);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setContentsMargins(11, 11, 11, 11);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        label = new QLabel(pw_repeat);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout2->addWidget(label);

        Edit_PwRepeat = new QLineEdit(pw_repeat);
        Edit_PwRepeat->setObjectName(QString::fromUtf8("Edit_PwRepeat"));
        Edit_PwRepeat->setFont(font);

        vboxLayout2->addWidget(Edit_PwRepeat);

        hboxLayout4 = new QHBoxLayout();
        hboxLayout4->setSpacing(6);
        hboxLayout4->setObjectName(QString::fromUtf8("hboxLayout4"));
        Button_Back = new QPushButton(pw_repeat);
        Button_Back->setObjectName(QString::fromUtf8("Button_Back"));

        hboxLayout4->addWidget(Button_Back);

        spacerItem3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout4->addItem(spacerItem3);

        Label_Unequal = new QLabel(pw_repeat);
        Label_Unequal->setObjectName(QString::fromUtf8("Label_Unequal"));
        QFont font1;
        font1.setPointSize(9);
        font1.setBold(true);
        font1.setWeight(75);
        Label_Unequal->setFont(font1);
        Label_Unequal->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        hboxLayout4->addWidget(Label_Unequal);


        vboxLayout2->addLayout(hboxLayout4);

        spacerItem4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout2->addItem(spacerItem4);

        stackedWidget->addWidget(pw_repeat);

        vboxLayout1->addWidget(stackedWidget);


        vboxLayout->addWidget(groupframe);

        buttonBox = new QDialogButtonBox(PasswordDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);

        QWidget::setTabOrder(Button_Bookmarks, Check_Password);
        QWidget::setTabOrder(Check_Password, Edit_Password);
        QWidget::setTabOrder(Edit_Password, ButtonChangeEchoMode);
        QWidget::setTabOrder(ButtonChangeEchoMode, Check_KeyFile);
        QWidget::setTabOrder(Check_KeyFile, Combo_KeyFile);
        QWidget::setTabOrder(Combo_KeyFile, Button_Browse);
        QWidget::setTabOrder(Button_Browse, Button_GenKeyFile);
        QWidget::setTabOrder(Button_GenKeyFile, Edit_PwRepeat);
        QWidget::setTabOrder(Edit_PwRepeat, Button_Back);
        QWidget::setTabOrder(Button_Back, buttonBox);

        retranslateUi(PasswordDlg);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(PasswordDlg);
    } // setupUi

    void retranslateUi(QDialog *PasswordDlg)
    {
        PasswordDlg->setWindowTitle(QString());
        Label_Bookmark->setText(QApplication::translate("PasswordDlg", "Last File", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("PasswordDlg", "Enter a Password and/or choose a key file.", 0, QApplication::UnicodeUTF8));
        groupframe->setTitle(QApplication::translate("PasswordDlg", "Key", 0, QApplication::UnicodeUTF8));
        Check_Password->setText(QApplication::translate("PasswordDlg", "Password:", 0, QApplication::UnicodeUTF8));
        Check_KeyFile->setText(QApplication::translate("PasswordDlg", "Key File:", 0, QApplication::UnicodeUTF8));
        Button_Browse->setText(QApplication::translate("PasswordDlg", "&Browse...", 0, QApplication::UnicodeUTF8));
        Button_GenKeyFile->setText(QApplication::translate("PasswordDlg", "Generate Key File...", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("PasswordDlg", "Please repeat your password:", 0, QApplication::UnicodeUTF8));
        Button_Back->setText(QApplication::translate("PasswordDlg", "Back", 0, QApplication::UnicodeUTF8));
        Label_Unequal->setText(QApplication::translate("PasswordDlg", "Passwords are not equal.", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class PasswordDlg: public Ui_PasswordDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PASSWORDDLG_H
