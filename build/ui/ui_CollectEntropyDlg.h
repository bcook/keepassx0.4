/********************************************************************************
** Form generated from reading UI file 'CollectEntropyDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COLLECTENTROPYDLG_H
#define UI_COLLECTENTROPYDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QProgressBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "lib/WaitAnimationWidget.h"

QT_BEGIN_NAMESPACE

class Ui_CollectEntropyDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QLabel *label;
    QProgressBar *progressBar;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem1;
    WaitAnimationWidget *Animation;
    QSpacerItem *spacerItem2;
    QWidget *page_2;
    QVBoxLayout *vboxLayout1;
    QLabel *Label_Finished;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CollectEntropyDlg)
    {
        if (CollectEntropyDlg->objectName().isEmpty())
            CollectEntropyDlg->setObjectName(QString::fromUtf8("CollectEntropyDlg"));
        CollectEntropyDlg->resize(432, 316);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(CollectEntropyDlg->sizePolicy().hasHeightForWidth());
        CollectEntropyDlg->setSizePolicy(sizePolicy);
        CollectEntropyDlg->setMinimumSize(QSize(432, 230));
        vboxLayout = new QVBoxLayout(CollectEntropyDlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        label = new QLabel(CollectEntropyDlg);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        label->setScaledContents(false);
        label->setWordWrap(true);

        vboxLayout->addWidget(label);

        progressBar = new QProgressBar(CollectEntropyDlg);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setMaximum(420);
        progressBar->setValue(0);
        progressBar->setOrientation(Qt::Horizontal);

        vboxLayout->addWidget(progressBar);

        stackedWidget = new QStackedWidget(CollectEntropyDlg);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy2);
        stackedWidget->setMinimumSize(QSize(16, 46));
        stackedWidget->setMaximumSize(QSize(16777215, 46));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        hboxLayout = new QHBoxLayout(page);
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem1);

        Animation = new WaitAnimationWidget(page);
        Animation->setObjectName(QString::fromUtf8("Animation"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(Animation->sizePolicy().hasHeightForWidth());
        Animation->setSizePolicy(sizePolicy3);
        Animation->setMinimumSize(QSize(41, 41));
        Animation->setMaximumSize(QSize(41, 41));

        hboxLayout->addWidget(Animation);

        spacerItem2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem2);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        vboxLayout1 = new QVBoxLayout(page_2);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        Label_Finished = new QLabel(page_2);
        Label_Finished->setObjectName(QString::fromUtf8("Label_Finished"));
        Label_Finished->setAlignment(Qt::AlignCenter);

        vboxLayout1->addWidget(Label_Finished);

        stackedWidget->addWidget(page_2);

        vboxLayout->addWidget(stackedWidget);

        buttonBox = new QDialogButtonBox(CollectEntropyDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(CollectEntropyDlg);
        QObject::connect(buttonBox, SIGNAL(rejected()), CollectEntropyDlg, SLOT(reject()));
        QObject::connect(buttonBox, SIGNAL(accepted()), CollectEntropyDlg, SLOT(accept()));

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CollectEntropyDlg);
    } // setupUi

    void retranslateUi(QDialog *CollectEntropyDlg)
    {
        CollectEntropyDlg->setWindowTitle(QApplication::translate("CollectEntropyDlg", "Random Number Generator", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("CollectEntropyDlg", "Collecting entropy...\n"
"Please move the mouse and/or press some keys until enought entropy for a reseed of the random number generator is collected.", 0, QApplication::UnicodeUTF8));
        Label_Finished->setText(QApplication::translate("CollectEntropyDlg", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Verdana'; font-size:9pt; font-weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600; color:#006400;\">Random pool successfully reseeded!</span></p></body></html>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CollectEntropyDlg: public Ui_CollectEntropyDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COLLECTENTROPYDLG_H
