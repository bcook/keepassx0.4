/********************************************************************************
** Form generated from reading UI file 'ManageBookmarksDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGEBOOKMARKSDLG_H
#define UI_MANAGEBOOKMARKSDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ManageBookmarksDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout;
    QListWidget *ListWidget;
    QVBoxLayout *vboxLayout1;
    QToolButton *Button_Add;
    QToolButton *Button_Edit;
    QToolButton *Button_Delete;
    QSpacerItem *spacerItem1;
    QToolButton *Button_Up;
    QToolButton *Button_Down;
    QSpacerItem *spacerItem2;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ManageBookmarksDlg)
    {
        if (ManageBookmarksDlg->objectName().isEmpty())
            ManageBookmarksDlg->setObjectName(QString::fromUtf8("ManageBookmarksDlg"));
        ManageBookmarksDlg->resize(280, 360);
        ManageBookmarksDlg->setMinimumSize(QSize(280, 300));
        vboxLayout = new QVBoxLayout(ManageBookmarksDlg);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        ListWidget = new QListWidget(ManageBookmarksDlg);
        ListWidget->setObjectName(QString::fromUtf8("ListWidget"));

        hboxLayout->addWidget(ListWidget);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        Button_Add = new QToolButton(ManageBookmarksDlg);
        Button_Add->setObjectName(QString::fromUtf8("Button_Add"));
        Button_Add->setIconSize(QSize(16, 16));

        vboxLayout1->addWidget(Button_Add);

        Button_Edit = new QToolButton(ManageBookmarksDlg);
        Button_Edit->setObjectName(QString::fromUtf8("Button_Edit"));
        Button_Edit->setIconSize(QSize(16, 16));

        vboxLayout1->addWidget(Button_Edit);

        Button_Delete = new QToolButton(ManageBookmarksDlg);
        Button_Delete->setObjectName(QString::fromUtf8("Button_Delete"));
        Button_Delete->setIconSize(QSize(16, 16));

        vboxLayout1->addWidget(Button_Delete);

        spacerItem1 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout1->addItem(spacerItem1);

        Button_Up = new QToolButton(ManageBookmarksDlg);
        Button_Up->setObjectName(QString::fromUtf8("Button_Up"));
        Button_Up->setIconSize(QSize(16, 16));

        vboxLayout1->addWidget(Button_Up);

        Button_Down = new QToolButton(ManageBookmarksDlg);
        Button_Down->setObjectName(QString::fromUtf8("Button_Down"));
        Button_Down->setIconSize(QSize(16, 16));

        vboxLayout1->addWidget(Button_Down);

        spacerItem2 = new QSpacerItem(20, 161, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout1->addItem(spacerItem2);


        hboxLayout->addLayout(vboxLayout1);


        vboxLayout->addLayout(hboxLayout);

        buttonBox = new QDialogButtonBox(ManageBookmarksDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        vboxLayout->addWidget(buttonBox);

        QWidget::setTabOrder(ListWidget, Button_Add);
        QWidget::setTabOrder(Button_Add, Button_Edit);
        QWidget::setTabOrder(Button_Edit, Button_Delete);
        QWidget::setTabOrder(Button_Delete, Button_Up);
        QWidget::setTabOrder(Button_Up, Button_Down);
        QWidget::setTabOrder(Button_Down, buttonBox);

        retranslateUi(ManageBookmarksDlg);

        QMetaObject::connectSlotsByName(ManageBookmarksDlg);
    } // setupUi

    void retranslateUi(QDialog *ManageBookmarksDlg)
    {
        ManageBookmarksDlg->setWindowTitle(QApplication::translate("ManageBookmarksDlg", "Manage Bookmarks", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ManageBookmarksDlg: public Ui_ManageBookmarksDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGEBOOKMARKSDLG_H
