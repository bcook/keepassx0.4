/********************************************************************************
** Form generated from reading UI file 'ExpiredEntriesDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXPIREDENTRIESDLG_H
#define UI_EXPIREDENTRIESDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ExpiredEntriesDialog
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QLabel *label;
    QTreeWidget *treeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ExpiredEntriesDialog)
    {
        if (ExpiredEntriesDialog->objectName().isEmpty())
            ExpiredEntriesDialog->setObjectName(QString::fromUtf8("ExpiredEntriesDialog"));
        ExpiredEntriesDialog->resize(588, 408);
        vboxLayout = new QVBoxLayout(ExpiredEntriesDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        label = new QLabel(ExpiredEntriesDialog);
        label->setObjectName(QString::fromUtf8("label"));

        vboxLayout->addWidget(label);

        treeWidget = new QTreeWidget(ExpiredEntriesDialog);
        treeWidget->setObjectName(QString::fromUtf8("treeWidget"));
        treeWidget->setRootIsDecorated(false);
        treeWidget->setUniformRowHeights(true);
        treeWidget->setItemsExpandable(false);
        treeWidget->setSortingEnabled(true);
        treeWidget->setAllColumnsShowFocus(true);

        vboxLayout->addWidget(treeWidget);

        buttonBox = new QDialogButtonBox(ExpiredEntriesDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(ExpiredEntriesDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ExpiredEntriesDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ExpiredEntriesDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ExpiredEntriesDialog);
    } // setupUi

    void retranslateUi(QDialog *ExpiredEntriesDialog)
    {
        ExpiredEntriesDialog->setWindowTitle(QApplication::translate("ExpiredEntriesDialog", "Expired Entries", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("ExpiredEntriesDialog", "Double click on an entry to jump to it.", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(3, QApplication::translate("ExpiredEntriesDialog", "Expired", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(2, QApplication::translate("ExpiredEntriesDialog", "Username", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(1, QApplication::translate("ExpiredEntriesDialog", "Title", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("ExpiredEntriesDialog", "Group", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class ExpiredEntriesDialog: public Ui_ExpiredEntriesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXPIREDENTRIESDLG_H
