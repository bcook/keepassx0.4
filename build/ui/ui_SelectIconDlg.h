/********************************************************************************
** Form generated from reading UI file 'SelectIconDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTICONDLG_H
#define UI_SELECTICONDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SelectIconDlg
{
public:
    QVBoxLayout *vboxLayout;
    QListWidget *List;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *SelectIconDlg)
    {
        if (SelectIconDlg->objectName().isEmpty())
            SelectIconDlg->setObjectName(QString::fromUtf8("SelectIconDlg"));
        SelectIconDlg->resize(478, 284);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SelectIconDlg->sizePolicy().hasHeightForWidth());
        SelectIconDlg->setSizePolicy(sizePolicy);
        vboxLayout = new QVBoxLayout(SelectIconDlg);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        List = new QListWidget(SelectIconDlg);
        List->setObjectName(QString::fromUtf8("List"));
        List->setIconSize(QSize(16, 16));
        List->setMovement(QListView::Static);
        List->setFlow(QListView::LeftToRight);
        List->setProperty("isWrapping", QVariant(true));
        List->setResizeMode(QListView::Fixed);
        List->setGridSize(QSize(32, 44));
        List->setViewMode(QListView::IconMode);

        vboxLayout->addWidget(List);

        ButtonBox = new QDialogButtonBox(SelectIconDlg);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Cancel);

        vboxLayout->addWidget(ButtonBox);


        retranslateUi(SelectIconDlg);

        QMetaObject::connectSlotsByName(SelectIconDlg);
    } // setupUi

    void retranslateUi(QDialog *SelectIconDlg)
    {
        SelectIconDlg->setWindowTitle(QApplication::translate("SelectIconDlg", "Icon Selection", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SelectIconDlg: public Ui_SelectIconDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTICONDLG_H
