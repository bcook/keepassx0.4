/********************************************************************************
** Form generated from reading UI file 'AddBookmarkDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDBOOKMARKDLG_H
#define UI_ADDBOOKMARKDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AddBookmarkDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *Edit_Title;
    QLabel *label_2;
    QLineEdit *Edit_Filename;
    QPushButton *Button_Browse;
    QFrame *line;
    QDialogButtonBox *buttonBox;

    void setupUi(QWidget *AddBookmarkDlg)
    {
        if (AddBookmarkDlg->objectName().isEmpty())
            AddBookmarkDlg->setObjectName(QString::fromUtf8("AddBookmarkDlg"));
        AddBookmarkDlg->resize(500, 180);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(AddBookmarkDlg->sizePolicy().hasHeightForWidth());
        AddBookmarkDlg->setSizePolicy(sizePolicy);
        AddBookmarkDlg->setMinimumSize(QSize(500, 180));
        AddBookmarkDlg->setMaximumSize(QSize(500, 180));
        vboxLayout = new QVBoxLayout(AddBookmarkDlg);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(AddBookmarkDlg);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        Edit_Title = new QLineEdit(AddBookmarkDlg);
        Edit_Title->setObjectName(QString::fromUtf8("Edit_Title"));

        gridLayout->addWidget(Edit_Title, 0, 1, 1, 1);

        label_2 = new QLabel(AddBookmarkDlg);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        Edit_Filename = new QLineEdit(AddBookmarkDlg);
        Edit_Filename->setObjectName(QString::fromUtf8("Edit_Filename"));

        gridLayout->addWidget(Edit_Filename, 1, 1, 1, 1);

        Button_Browse = new QPushButton(AddBookmarkDlg);
        Button_Browse->setObjectName(QString::fromUtf8("Button_Browse"));

        gridLayout->addWidget(Button_Browse, 1, 2, 1, 1);


        vboxLayout->addLayout(gridLayout);

        line = new QFrame(AddBookmarkDlg);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        vboxLayout->addWidget(line);

        buttonBox = new QDialogButtonBox(AddBookmarkDlg);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);

        QWidget::setTabOrder(Edit_Title, Edit_Filename);
        QWidget::setTabOrder(Edit_Filename, Button_Browse);
        QWidget::setTabOrder(Button_Browse, buttonBox);

        retranslateUi(AddBookmarkDlg);

        QMetaObject::connectSlotsByName(AddBookmarkDlg);
    } // setupUi

    void retranslateUi(QWidget *AddBookmarkDlg)
    {
        AddBookmarkDlg->setWindowTitle(QApplication::translate("AddBookmarkDlg", "Add Bookmark", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("AddBookmarkDlg", "Title:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("AddBookmarkDlg", "File:", 0, QApplication::UnicodeUTF8));
        Button_Browse->setText(QApplication::translate("AddBookmarkDlg", "Browse...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AddBookmarkDlg: public Ui_AddBookmarkDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDBOOKMARKDLG_H
