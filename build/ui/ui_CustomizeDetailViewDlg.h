/********************************************************************************
** Form generated from reading UI file 'CustomizeDetailViewDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUSTOMIZEDETAILVIEWDLG_H
#define UI_CUSTOMIZEDETAILVIEWDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CustomizeDetailViewDialog
{
public:
    QVBoxLayout *vboxLayout;
    QTabWidget *TabWidget;
    QWidget *tab;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QToolButton *BtnBold;
    QToolButton *BtnItalic;
    QToolButton *BtnUnderline;
    QToolButton *BtnAlignLeft;
    QToolButton *BtnAlignCenter;
    QToolButton *BtnAlignRight;
    QToolButton *BtnAlignBlock;
    QToolButton *BtnColor;
    QComboBox *FontSize;
    QToolButton *BtnTemplates;
    QSpacerItem *spacerItem;
    QTextEdit *RichEdit;
    QWidget *tab_2;
    QVBoxLayout *vboxLayout2;
    QTextEdit *HtmlEdit;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *CustomizeDetailViewDialog)
    {
        if (CustomizeDetailViewDialog->objectName().isEmpty())
            CustomizeDetailViewDialog->setObjectName(QString::fromUtf8("CustomizeDetailViewDialog"));
        CustomizeDetailViewDialog->resize(531, 402);
        vboxLayout = new QVBoxLayout(CustomizeDetailViewDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        TabWidget = new QTabWidget(CustomizeDetailViewDialog);
        TabWidget->setObjectName(QString::fromUtf8("TabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        tab->setGeometry(QRect(0, 0, 509, 324));
        vboxLayout1 = new QVBoxLayout(tab);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        BtnBold = new QToolButton(tab);
        BtnBold->setObjectName(QString::fromUtf8("BtnBold"));
        BtnBold->setIconSize(QSize(16, 16));
        BtnBold->setCheckable(true);

        hboxLayout->addWidget(BtnBold);

        BtnItalic = new QToolButton(tab);
        BtnItalic->setObjectName(QString::fromUtf8("BtnItalic"));
        BtnItalic->setCheckable(true);

        hboxLayout->addWidget(BtnItalic);

        BtnUnderline = new QToolButton(tab);
        BtnUnderline->setObjectName(QString::fromUtf8("BtnUnderline"));
        BtnUnderline->setCheckable(true);

        hboxLayout->addWidget(BtnUnderline);

        BtnAlignLeft = new QToolButton(tab);
        BtnAlignLeft->setObjectName(QString::fromUtf8("BtnAlignLeft"));
        BtnAlignLeft->setCheckable(true);

        hboxLayout->addWidget(BtnAlignLeft);

        BtnAlignCenter = new QToolButton(tab);
        BtnAlignCenter->setObjectName(QString::fromUtf8("BtnAlignCenter"));
        BtnAlignCenter->setCheckable(true);

        hboxLayout->addWidget(BtnAlignCenter);

        BtnAlignRight = new QToolButton(tab);
        BtnAlignRight->setObjectName(QString::fromUtf8("BtnAlignRight"));
        BtnAlignRight->setCheckable(true);

        hboxLayout->addWidget(BtnAlignRight);

        BtnAlignBlock = new QToolButton(tab);
        BtnAlignBlock->setObjectName(QString::fromUtf8("BtnAlignBlock"));
        BtnAlignBlock->setCheckable(true);

        hboxLayout->addWidget(BtnAlignBlock);

        BtnColor = new QToolButton(tab);
        BtnColor->setObjectName(QString::fromUtf8("BtnColor"));

        hboxLayout->addWidget(BtnColor);

        FontSize = new QComboBox(tab);
        FontSize->setObjectName(QString::fromUtf8("FontSize"));
        FontSize->setEditable(true);

        hboxLayout->addWidget(FontSize);

        BtnTemplates = new QToolButton(tab);
        BtnTemplates->setObjectName(QString::fromUtf8("BtnTemplates"));
        BtnTemplates->setPopupMode(QToolButton::InstantPopup);
        BtnTemplates->setArrowType(Qt::NoArrow);

        hboxLayout->addWidget(BtnTemplates);

        spacerItem = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);


        vboxLayout1->addLayout(hboxLayout);

        RichEdit = new QTextEdit(tab);
        RichEdit->setObjectName(QString::fromUtf8("RichEdit"));

        vboxLayout1->addWidget(RichEdit);

        TabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tab_2->setGeometry(QRect(0, 0, 509, 324));
        vboxLayout2 = new QVBoxLayout(tab_2);
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        HtmlEdit = new QTextEdit(tab_2);
        HtmlEdit->setObjectName(QString::fromUtf8("HtmlEdit"));
        HtmlEdit->setAcceptRichText(false);

        vboxLayout2->addWidget(HtmlEdit);

        TabWidget->addTab(tab_2, QString());

        vboxLayout->addWidget(TabWidget);

        ButtonBox = new QDialogButtonBox(CustomizeDetailViewDialog);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok|QDialogButtonBox::RestoreDefaults);

        vboxLayout->addWidget(ButtonBox);

        QWidget::setTabOrder(TabWidget, BtnBold);
        QWidget::setTabOrder(BtnBold, BtnItalic);
        QWidget::setTabOrder(BtnItalic, BtnUnderline);
        QWidget::setTabOrder(BtnUnderline, BtnAlignLeft);
        QWidget::setTabOrder(BtnAlignLeft, BtnAlignCenter);
        QWidget::setTabOrder(BtnAlignCenter, BtnAlignRight);
        QWidget::setTabOrder(BtnAlignRight, BtnAlignBlock);
        QWidget::setTabOrder(BtnAlignBlock, BtnColor);
        QWidget::setTabOrder(BtnColor, FontSize);
        QWidget::setTabOrder(FontSize, BtnTemplates);
        QWidget::setTabOrder(BtnTemplates, RichEdit);
        QWidget::setTabOrder(RichEdit, HtmlEdit);
        QWidget::setTabOrder(HtmlEdit, ButtonBox);

        retranslateUi(CustomizeDetailViewDialog);

        TabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CustomizeDetailViewDialog);
    } // setupUi

    void retranslateUi(QDialog *CustomizeDetailViewDialog)
    {
        CustomizeDetailViewDialog->setWindowTitle(QApplication::translate("CustomizeDetailViewDialog", "Dialog", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnBold->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Bold", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnBold->setText(QApplication::translate("CustomizeDetailViewDialog", "B", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnItalic->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Italic", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnItalic->setText(QApplication::translate("CustomizeDetailViewDialog", "I", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnUnderline->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Underlined", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnUnderline->setText(QApplication::translate("CustomizeDetailViewDialog", "U", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnAlignLeft->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Left-Aligned", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnAlignLeft->setText(QApplication::translate("CustomizeDetailViewDialog", "L", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnAlignCenter->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Centered", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnAlignCenter->setText(QApplication::translate("CustomizeDetailViewDialog", "C", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnAlignRight->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Right-Aligned", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnAlignRight->setText(QApplication::translate("CustomizeDetailViewDialog", "R", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnAlignBlock->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Justified", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnAlignBlock->setText(QApplication::translate("CustomizeDetailViewDialog", "B", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        BtnColor->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Text Color", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnColor->setText(QApplication::translate("CustomizeDetailViewDialog", "C", 0, QApplication::UnicodeUTF8));
        FontSize->clear();
        FontSize->insertItems(0, QStringList()
         << QApplication::translate("CustomizeDetailViewDialog", "6", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "7", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "8", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "9", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "10", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "11", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "12", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "14", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "16", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "18", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "20", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "22", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "24", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "26", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "28", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "36", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "42", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("CustomizeDetailViewDialog", "78", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_TOOLTIP
        FontSize->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Font Size", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_TOOLTIP
        BtnTemplates->setToolTip(QApplication::translate("CustomizeDetailViewDialog", "Templates", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        BtnTemplates->setText(QApplication::translate("CustomizeDetailViewDialog", "T", 0, QApplication::UnicodeUTF8));
        TabWidget->setTabText(TabWidget->indexOf(tab), QApplication::translate("CustomizeDetailViewDialog", "Rich Text Editor", 0, QApplication::UnicodeUTF8));
        TabWidget->setTabText(TabWidget->indexOf(tab_2), QApplication::translate("CustomizeDetailViewDialog", "HTML", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CustomizeDetailViewDialog: public Ui_CustomizeDetailViewDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUSTOMIZEDETAILVIEWDLG_H
