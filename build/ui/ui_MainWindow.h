/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QSplitter>
#include <QtGui/QTextBrowser>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "../../src/lib/EntryView.h"
#include "../../src/lib/GroupView.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *ManageBookmarksAction;
    QAction *FileOpenAction;
    QAction *FileCloseAction;
    QAction *FileSaveAction;
    QAction *FileSaveAsAction;
    QAction *FileSettingsAction;
    QAction *FileChangeKeyAction;
    QAction *FileUnLockWorkspaceAction;
    QAction *FileExitAction;
    QAction *EditNewSubgroupAction;
    QAction *EditEditGroupAction;
    QAction *EditDeleteGroupAction;
    QAction *EditPasswordToClipboardAction;
    QAction *EditUsernameToClipboardAction;
    QAction *EditOpenUrlAction;
    QAction *EditSaveAttachmentAction;
    QAction *EditNewEntryAction;
    QAction *EditEditEntryAction;
    QAction *EditDeleteEntryAction;
    QAction *EditCloneEntryAction;
    QAction *EditSearchAction;
    QAction *EditGroupSearchAction;
    QAction *ViewShowEntryDetailsAction;
    QAction *ViewHideUsernamesAction;
    QAction *ViewHidePasswordsAction;
    QAction *ViewColumnsTitleAction;
    QAction *ViewColumnsUsernameAction;
    QAction *ViewColumnsUrlAction;
    QAction *ViewColumnsPasswordAction;
    QAction *ViewColumnsCommentAction;
    QAction *ViewColumnsExpireAction;
    QAction *ViewColumnsCreationAction;
    QAction *ViewColumnsLastChangeAction;
    QAction *ViewColumnsLastAccessAction;
    QAction *ViewColumnsAttachmentAction;
    QAction *ExtrasSettingsAction;
    QAction *HelpAboutAction;
    QAction *ViewShowStatusbarAction;
    QAction *HelpHandbookAction;
    QAction *HideSearchResultsAction;
    QAction *EditAutoTypeAction;
    QAction *ViewToolButtonSize16Action;
    QAction *ViewToolButtonSize22Action;
    QAction *ViewToolButtonSize28Action;
    QAction *FileNewAction;
    QAction *ExtrasPasswordGenAction;
    QAction *ViewColumnsGroupAction;
    QAction *ExtrasShowExpiredEntriesAction;
    QAction *ExtrasTrashCanAction;
    QAction *AddBookmarkAction;
    QAction *AddThisAsBookmarkAction;
    QAction *EditCopyUrlAction;
    QAction *EditNewGroupAction;
    QAction *EditGroupSortAction;
    QAction *ViewMinimizeAction;
    QWidget *centralWidget;
    QVBoxLayout *vboxLayout;
    QSplitter *VSplitter;
    KeepassGroupView *GroupView;
    QSplitter *HSplitter;
    KeepassEntryView *EntryView;
    QTextBrowser *DetailView;
    QMenuBar *menuBar;
    QMenu *menuHelp;
    QMenu *menuFile;
    QMenu *menuExport;
    QMenu *menuImport;
    QMenu *menuBookmarks;
    QMenu *menuEntries;
    QMenu *ViewMenu;
    QMenu *menuTool_Button_Sizes;
    QMenu *menuColumns;
    QMenu *menuExtras;
    QMenu *menuGroups;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(724, 468);
        MainWindow->setMouseTracking(true);
        ManageBookmarksAction = new QAction(MainWindow);
        ManageBookmarksAction->setObjectName(QString::fromUtf8("ManageBookmarksAction"));
        FileOpenAction = new QAction(MainWindow);
        FileOpenAction->setObjectName(QString::fromUtf8("FileOpenAction"));
        FileCloseAction = new QAction(MainWindow);
        FileCloseAction->setObjectName(QString::fromUtf8("FileCloseAction"));
        FileSaveAction = new QAction(MainWindow);
        FileSaveAction->setObjectName(QString::fromUtf8("FileSaveAction"));
        FileSaveAsAction = new QAction(MainWindow);
        FileSaveAsAction->setObjectName(QString::fromUtf8("FileSaveAsAction"));
        FileSettingsAction = new QAction(MainWindow);
        FileSettingsAction->setObjectName(QString::fromUtf8("FileSettingsAction"));
        FileChangeKeyAction = new QAction(MainWindow);
        FileChangeKeyAction->setObjectName(QString::fromUtf8("FileChangeKeyAction"));
        FileUnLockWorkspaceAction = new QAction(MainWindow);
        FileUnLockWorkspaceAction->setObjectName(QString::fromUtf8("FileUnLockWorkspaceAction"));
        FileExitAction = new QAction(MainWindow);
        FileExitAction->setObjectName(QString::fromUtf8("FileExitAction"));
        FileExitAction->setMenuRole(QAction::QuitRole);
        EditNewSubgroupAction = new QAction(MainWindow);
        EditNewSubgroupAction->setObjectName(QString::fromUtf8("EditNewSubgroupAction"));
        EditEditGroupAction = new QAction(MainWindow);
        EditEditGroupAction->setObjectName(QString::fromUtf8("EditEditGroupAction"));
        EditDeleteGroupAction = new QAction(MainWindow);
        EditDeleteGroupAction->setObjectName(QString::fromUtf8("EditDeleteGroupAction"));
        EditPasswordToClipboardAction = new QAction(MainWindow);
        EditPasswordToClipboardAction->setObjectName(QString::fromUtf8("EditPasswordToClipboardAction"));
        EditUsernameToClipboardAction = new QAction(MainWindow);
        EditUsernameToClipboardAction->setObjectName(QString::fromUtf8("EditUsernameToClipboardAction"));
        EditOpenUrlAction = new QAction(MainWindow);
        EditOpenUrlAction->setObjectName(QString::fromUtf8("EditOpenUrlAction"));
        EditSaveAttachmentAction = new QAction(MainWindow);
        EditSaveAttachmentAction->setObjectName(QString::fromUtf8("EditSaveAttachmentAction"));
        EditNewEntryAction = new QAction(MainWindow);
        EditNewEntryAction->setObjectName(QString::fromUtf8("EditNewEntryAction"));
        EditEditEntryAction = new QAction(MainWindow);
        EditEditEntryAction->setObjectName(QString::fromUtf8("EditEditEntryAction"));
        EditDeleteEntryAction = new QAction(MainWindow);
        EditDeleteEntryAction->setObjectName(QString::fromUtf8("EditDeleteEntryAction"));
        EditCloneEntryAction = new QAction(MainWindow);
        EditCloneEntryAction->setObjectName(QString::fromUtf8("EditCloneEntryAction"));
        EditSearchAction = new QAction(MainWindow);
        EditSearchAction->setObjectName(QString::fromUtf8("EditSearchAction"));
        EditGroupSearchAction = new QAction(MainWindow);
        EditGroupSearchAction->setObjectName(QString::fromUtf8("EditGroupSearchAction"));
        ViewShowEntryDetailsAction = new QAction(MainWindow);
        ViewShowEntryDetailsAction->setObjectName(QString::fromUtf8("ViewShowEntryDetailsAction"));
        ViewShowEntryDetailsAction->setCheckable(true);
        ViewHideUsernamesAction = new QAction(MainWindow);
        ViewHideUsernamesAction->setObjectName(QString::fromUtf8("ViewHideUsernamesAction"));
        ViewHideUsernamesAction->setCheckable(true);
        ViewHidePasswordsAction = new QAction(MainWindow);
        ViewHidePasswordsAction->setObjectName(QString::fromUtf8("ViewHidePasswordsAction"));
        ViewHidePasswordsAction->setCheckable(true);
        ViewColumnsTitleAction = new QAction(MainWindow);
        ViewColumnsTitleAction->setObjectName(QString::fromUtf8("ViewColumnsTitleAction"));
        ViewColumnsTitleAction->setCheckable(true);
        ViewColumnsUsernameAction = new QAction(MainWindow);
        ViewColumnsUsernameAction->setObjectName(QString::fromUtf8("ViewColumnsUsernameAction"));
        ViewColumnsUsernameAction->setCheckable(true);
        ViewColumnsUrlAction = new QAction(MainWindow);
        ViewColumnsUrlAction->setObjectName(QString::fromUtf8("ViewColumnsUrlAction"));
        ViewColumnsUrlAction->setCheckable(true);
        ViewColumnsPasswordAction = new QAction(MainWindow);
        ViewColumnsPasswordAction->setObjectName(QString::fromUtf8("ViewColumnsPasswordAction"));
        ViewColumnsPasswordAction->setCheckable(true);
        ViewColumnsCommentAction = new QAction(MainWindow);
        ViewColumnsCommentAction->setObjectName(QString::fromUtf8("ViewColumnsCommentAction"));
        ViewColumnsCommentAction->setCheckable(true);
        ViewColumnsExpireAction = new QAction(MainWindow);
        ViewColumnsExpireAction->setObjectName(QString::fromUtf8("ViewColumnsExpireAction"));
        ViewColumnsExpireAction->setCheckable(true);
        ViewColumnsCreationAction = new QAction(MainWindow);
        ViewColumnsCreationAction->setObjectName(QString::fromUtf8("ViewColumnsCreationAction"));
        ViewColumnsCreationAction->setCheckable(true);
        ViewColumnsLastChangeAction = new QAction(MainWindow);
        ViewColumnsLastChangeAction->setObjectName(QString::fromUtf8("ViewColumnsLastChangeAction"));
        ViewColumnsLastChangeAction->setCheckable(true);
        ViewColumnsLastAccessAction = new QAction(MainWindow);
        ViewColumnsLastAccessAction->setObjectName(QString::fromUtf8("ViewColumnsLastAccessAction"));
        ViewColumnsLastAccessAction->setCheckable(true);
        ViewColumnsAttachmentAction = new QAction(MainWindow);
        ViewColumnsAttachmentAction->setObjectName(QString::fromUtf8("ViewColumnsAttachmentAction"));
        ViewColumnsAttachmentAction->setCheckable(true);
        ExtrasSettingsAction = new QAction(MainWindow);
        ExtrasSettingsAction->setObjectName(QString::fromUtf8("ExtrasSettingsAction"));
        ExtrasSettingsAction->setMenuRole(QAction::PreferencesRole);
        HelpAboutAction = new QAction(MainWindow);
        HelpAboutAction->setObjectName(QString::fromUtf8("HelpAboutAction"));
        HelpAboutAction->setMenuRole(QAction::AboutRole);
        ViewShowStatusbarAction = new QAction(MainWindow);
        ViewShowStatusbarAction->setObjectName(QString::fromUtf8("ViewShowStatusbarAction"));
        ViewShowStatusbarAction->setCheckable(true);
        HelpHandbookAction = new QAction(MainWindow);
        HelpHandbookAction->setObjectName(QString::fromUtf8("HelpHandbookAction"));
        HideSearchResultsAction = new QAction(MainWindow);
        HideSearchResultsAction->setObjectName(QString::fromUtf8("HideSearchResultsAction"));
        EditAutoTypeAction = new QAction(MainWindow);
        EditAutoTypeAction->setObjectName(QString::fromUtf8("EditAutoTypeAction"));
        ViewToolButtonSize16Action = new QAction(MainWindow);
        ViewToolButtonSize16Action->setObjectName(QString::fromUtf8("ViewToolButtonSize16Action"));
        ViewToolButtonSize16Action->setCheckable(true);
        ViewToolButtonSize22Action = new QAction(MainWindow);
        ViewToolButtonSize22Action->setObjectName(QString::fromUtf8("ViewToolButtonSize22Action"));
        ViewToolButtonSize22Action->setCheckable(true);
        ViewToolButtonSize28Action = new QAction(MainWindow);
        ViewToolButtonSize28Action->setObjectName(QString::fromUtf8("ViewToolButtonSize28Action"));
        ViewToolButtonSize28Action->setCheckable(true);
        FileNewAction = new QAction(MainWindow);
        FileNewAction->setObjectName(QString::fromUtf8("FileNewAction"));
        ExtrasPasswordGenAction = new QAction(MainWindow);
        ExtrasPasswordGenAction->setObjectName(QString::fromUtf8("ExtrasPasswordGenAction"));
        ViewColumnsGroupAction = new QAction(MainWindow);
        ViewColumnsGroupAction->setObjectName(QString::fromUtf8("ViewColumnsGroupAction"));
        ViewColumnsGroupAction->setCheckable(true);
        ViewColumnsGroupAction->setVisible(false);
        ExtrasShowExpiredEntriesAction = new QAction(MainWindow);
        ExtrasShowExpiredEntriesAction->setObjectName(QString::fromUtf8("ExtrasShowExpiredEntriesAction"));
        ExtrasTrashCanAction = new QAction(MainWindow);
        ExtrasTrashCanAction->setObjectName(QString::fromUtf8("ExtrasTrashCanAction"));
        ExtrasTrashCanAction->setVisible(false);
        AddBookmarkAction = new QAction(MainWindow);
        AddBookmarkAction->setObjectName(QString::fromUtf8("AddBookmarkAction"));
        AddThisAsBookmarkAction = new QAction(MainWindow);
        AddThisAsBookmarkAction->setObjectName(QString::fromUtf8("AddThisAsBookmarkAction"));
        EditCopyUrlAction = new QAction(MainWindow);
        EditCopyUrlAction->setObjectName(QString::fromUtf8("EditCopyUrlAction"));
        EditNewGroupAction = new QAction(MainWindow);
        EditNewGroupAction->setObjectName(QString::fromUtf8("EditNewGroupAction"));
        EditGroupSortAction = new QAction(MainWindow);
        EditGroupSortAction->setObjectName(QString::fromUtf8("EditGroupSortAction"));
        ViewMinimizeAction = new QAction(MainWindow);
        ViewMinimizeAction->setObjectName(QString::fromUtf8("ViewMinimizeAction"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        vboxLayout = new QVBoxLayout(centralWidget);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        VSplitter = new QSplitter(centralWidget);
        VSplitter->setObjectName(QString::fromUtf8("VSplitter"));
        VSplitter->setOrientation(Qt::Horizontal);
        GroupView = new KeepassGroupView(VSplitter);
        GroupView->setObjectName(QString::fromUtf8("GroupView"));
        GroupView->setEnabled(false);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(30);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GroupView->sizePolicy().hasHeightForWidth());
        GroupView->setSizePolicy(sizePolicy);
        GroupView->setAcceptDrops(true);
        GroupView->setProperty("showDropIndicator", QVariant(true));
        GroupView->setDragEnabled(false);
        VSplitter->addWidget(GroupView);
        HSplitter = new QSplitter(VSplitter);
        HSplitter->setObjectName(QString::fromUtf8("HSplitter"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(70);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(HSplitter->sizePolicy().hasHeightForWidth());
        HSplitter->setSizePolicy(sizePolicy1);
        HSplitter->setOrientation(Qt::Vertical);
        EntryView = new KeepassEntryView(HSplitter);
        EntryView->setObjectName(QString::fromUtf8("EntryView"));
        EntryView->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(70);
        sizePolicy2.setHeightForWidth(EntryView->sizePolicy().hasHeightForWidth());
        EntryView->setSizePolicy(sizePolicy2);
        EntryView->setSelectionMode(QAbstractItemView::ExtendedSelection);
        EntryView->setRootIsDecorated(false);
        EntryView->setSortingEnabled(true);
        HSplitter->addWidget(EntryView);
        DetailView = new QTextBrowser(HSplitter);
        DetailView->setObjectName(QString::fromUtf8("DetailView"));
        DetailView->setEnabled(false);
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(30);
        sizePolicy3.setHeightForWidth(DetailView->sizePolicy().hasHeightForWidth());
        DetailView->setSizePolicy(sizePolicy3);
        DetailView->setMinimumSize(QSize(0, 30));
        DetailView->setReadOnly(true);
        DetailView->setOpenLinks(false);
        HSplitter->addWidget(DetailView);
        VSplitter->addWidget(HSplitter);

        vboxLayout->addWidget(VSplitter);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 724, 24));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        menuExport = new QMenu(menuFile);
        menuExport->setObjectName(QString::fromUtf8("menuExport"));
        menuImport = new QMenu(menuFile);
        menuImport->setObjectName(QString::fromUtf8("menuImport"));
        menuBookmarks = new QMenu(menuFile);
        menuBookmarks->setObjectName(QString::fromUtf8("menuBookmarks"));
        menuEntries = new QMenu(menuBar);
        menuEntries->setObjectName(QString::fromUtf8("menuEntries"));
        ViewMenu = new QMenu(menuBar);
        ViewMenu->setObjectName(QString::fromUtf8("ViewMenu"));
        menuTool_Button_Sizes = new QMenu(ViewMenu);
        menuTool_Button_Sizes->setObjectName(QString::fromUtf8("menuTool_Button_Sizes"));
        menuColumns = new QMenu(ViewMenu);
        menuColumns->setObjectName(QString::fromUtf8("menuColumns"));
        menuExtras = new QMenu(menuBar);
        menuExtras->setObjectName(QString::fromUtf8("menuExtras"));
        menuGroups = new QMenu(menuBar);
        menuGroups->setObjectName(QString::fromUtf8("menuGroups"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuEntries->menuAction());
        menuBar->addAction(menuGroups->menuAction());
        menuBar->addAction(ViewMenu->menuAction());
        menuBar->addAction(menuExtras->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuHelp->addAction(HelpHandbookAction);
        menuHelp->addSeparator();
        menuHelp->addAction(HelpAboutAction);
        menuFile->addAction(FileNewAction);
        menuFile->addAction(FileOpenAction);
        menuFile->addAction(menuBookmarks->menuAction());
        menuFile->addAction(FileCloseAction);
        menuFile->addSeparator();
        menuFile->addAction(FileSaveAction);
        menuFile->addAction(FileSaveAsAction);
        menuFile->addSeparator();
        menuFile->addAction(FileSettingsAction);
        menuFile->addAction(FileChangeKeyAction);
        menuFile->addSeparator();
        menuFile->addAction(menuImport->menuAction());
        menuFile->addAction(menuExport->menuAction());
        menuFile->addSeparator();
        menuFile->addAction(FileUnLockWorkspaceAction);
        menuFile->addAction(FileExitAction);
        menuEntries->addAction(EditNewEntryAction);
        menuEntries->addAction(EditCloneEntryAction);
        menuEntries->addAction(EditEditEntryAction);
        menuEntries->addAction(EditDeleteEntryAction);
        menuEntries->addAction(EditAutoTypeAction);
        menuEntries->addSeparator();
        menuEntries->addAction(EditUsernameToClipboardAction);
        menuEntries->addAction(EditPasswordToClipboardAction);
        menuEntries->addAction(EditOpenUrlAction);
        menuEntries->addAction(EditCopyUrlAction);
        menuEntries->addAction(EditSaveAttachmentAction);
        menuEntries->addSeparator();
        menuEntries->addAction(EditSearchAction);
        menuEntries->addAction(EditGroupSearchAction);
        ViewMenu->addAction(ViewShowEntryDetailsAction);
        ViewMenu->addAction(ViewShowStatusbarAction);
        ViewMenu->addSeparator();
        ViewMenu->addAction(ViewHideUsernamesAction);
        ViewMenu->addAction(ViewHidePasswordsAction);
        ViewMenu->addSeparator();
        ViewMenu->addAction(menuColumns->menuAction());
        ViewMenu->addAction(menuTool_Button_Sizes->menuAction());
        menuTool_Button_Sizes->addAction(ViewToolButtonSize16Action);
        menuTool_Button_Sizes->addAction(ViewToolButtonSize22Action);
        menuTool_Button_Sizes->addAction(ViewToolButtonSize28Action);
        menuColumns->addAction(ViewColumnsGroupAction);
        menuColumns->addAction(ViewColumnsTitleAction);
        menuColumns->addAction(ViewColumnsUsernameAction);
        menuColumns->addAction(ViewColumnsUrlAction);
        menuColumns->addAction(ViewColumnsPasswordAction);
        menuColumns->addAction(ViewColumnsCommentAction);
        menuColumns->addSeparator();
        menuColumns->addAction(ViewColumnsExpireAction);
        menuColumns->addAction(ViewColumnsCreationAction);
        menuColumns->addAction(ViewColumnsLastChangeAction);
        menuColumns->addAction(ViewColumnsLastAccessAction);
        menuColumns->addAction(ViewColumnsAttachmentAction);
        menuExtras->addAction(ExtrasPasswordGenAction);
        menuExtras->addAction(ExtrasShowExpiredEntriesAction);
        menuExtras->addAction(ExtrasTrashCanAction);
        menuExtras->addSeparator();
        menuExtras->addAction(ExtrasSettingsAction);
        menuGroups->addAction(EditNewGroupAction);
        menuGroups->addAction(EditNewSubgroupAction);
        menuGroups->addAction(EditEditGroupAction);
        menuGroups->addAction(EditDeleteGroupAction);
        menuGroups->addAction(EditGroupSortAction);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "KeePassX", 0, QApplication::UnicodeUTF8));
        ManageBookmarksAction->setText(QApplication::translate("MainWindow", "&Manage Bookmarks...", 0, QApplication::UnicodeUTF8));
        FileOpenAction->setText(QApplication::translate("MainWindow", "&Open Database...", 0, QApplication::UnicodeUTF8));
        FileCloseAction->setText(QApplication::translate("MainWindow", "&Close Database", 0, QApplication::UnicodeUTF8));
        FileSaveAction->setText(QApplication::translate("MainWindow", "&Save Database", 0, QApplication::UnicodeUTF8));
        FileSaveAsAction->setText(QApplication::translate("MainWindow", "Save Database &As...", 0, QApplication::UnicodeUTF8));
        FileSettingsAction->setText(QApplication::translate("MainWindow", "&Database Settings...", 0, QApplication::UnicodeUTF8));
        FileChangeKeyAction->setText(QApplication::translate("MainWindow", "Change &Master Key...", 0, QApplication::UnicodeUTF8));
        FileUnLockWorkspaceAction->setText(QApplication::translate("MainWindow", "&Lock Workspace", 0, QApplication::UnicodeUTF8));
        FileExitAction->setText(QApplication::translate("MainWindow", "&Quit", 0, QApplication::UnicodeUTF8));
        EditNewSubgroupAction->setText(QApplication::translate("MainWindow", "&Add New Subgroup...", 0, QApplication::UnicodeUTF8));
        EditEditGroupAction->setText(QApplication::translate("MainWindow", "&Edit Group...", 0, QApplication::UnicodeUTF8));
        EditDeleteGroupAction->setText(QApplication::translate("MainWindow", "&Delete Group", 0, QApplication::UnicodeUTF8));
        EditPasswordToClipboardAction->setText(QApplication::translate("MainWindow", "Copy Password &to Clipboard", 0, QApplication::UnicodeUTF8));
        EditUsernameToClipboardAction->setText(QApplication::translate("MainWindow", "Copy &Username to Clipboard", 0, QApplication::UnicodeUTF8));
        EditOpenUrlAction->setText(QApplication::translate("MainWindow", "&Open URL", 0, QApplication::UnicodeUTF8));
        EditSaveAttachmentAction->setText(QApplication::translate("MainWindow", "&Save Attachment As...", 0, QApplication::UnicodeUTF8));
        EditNewEntryAction->setText(QApplication::translate("MainWindow", "Add &New Entry...", 0, QApplication::UnicodeUTF8));
        EditEditEntryAction->setText(QApplication::translate("MainWindow", "&View/Edit Entry...", 0, QApplication::UnicodeUTF8));
        EditDeleteEntryAction->setText(QApplication::translate("MainWindow", "De&lete Entry", 0, QApplication::UnicodeUTF8));
        EditCloneEntryAction->setText(QApplication::translate("MainWindow", "&Clone Entry", 0, QApplication::UnicodeUTF8));
        EditSearchAction->setText(QApplication::translate("MainWindow", "Search &in Database...", 0, QApplication::UnicodeUTF8));
        EditGroupSearchAction->setText(QApplication::translate("MainWindow", "Search in this &Group...", 0, QApplication::UnicodeUTF8));
        ViewShowEntryDetailsAction->setText(QApplication::translate("MainWindow", "Show &Entry Details", 0, QApplication::UnicodeUTF8));
        ViewHideUsernamesAction->setText(QApplication::translate("MainWindow", "Hide &Usernames", 0, QApplication::UnicodeUTF8));
        ViewHidePasswordsAction->setText(QApplication::translate("MainWindow", "Hide &Passwords", 0, QApplication::UnicodeUTF8));
        ViewColumnsTitleAction->setText(QApplication::translate("MainWindow", "&Title", 0, QApplication::UnicodeUTF8));
        ViewColumnsUsernameAction->setText(QApplication::translate("MainWindow", "User&name", 0, QApplication::UnicodeUTF8));
        ViewColumnsUrlAction->setText(QApplication::translate("MainWindow", "&URL", 0, QApplication::UnicodeUTF8));
        ViewColumnsPasswordAction->setText(QApplication::translate("MainWindow", "&Password", 0, QApplication::UnicodeUTF8));
        ViewColumnsCommentAction->setText(QApplication::translate("MainWindow", "&Comment", 0, QApplication::UnicodeUTF8));
        ViewColumnsExpireAction->setText(QApplication::translate("MainWindow", "E&xpires", 0, QApplication::UnicodeUTF8));
        ViewColumnsCreationAction->setText(QApplication::translate("MainWindow", "C&reation", 0, QApplication::UnicodeUTF8));
        ViewColumnsLastChangeAction->setText(QApplication::translate("MainWindow", "&Last Change", 0, QApplication::UnicodeUTF8));
        ViewColumnsLastAccessAction->setText(QApplication::translate("MainWindow", "Last &Access", 0, QApplication::UnicodeUTF8));
        ViewColumnsAttachmentAction->setText(QApplication::translate("MainWindow", "A&ttachment", 0, QApplication::UnicodeUTF8));
        ExtrasSettingsAction->setText(QApplication::translate("MainWindow", "&Settings...", 0, QApplication::UnicodeUTF8));
        HelpAboutAction->setText(QApplication::translate("MainWindow", "&About...", 0, QApplication::UnicodeUTF8));
        ViewShowStatusbarAction->setText(QApplication::translate("MainWindow", "Show &Statusbar", 0, QApplication::UnicodeUTF8));
        HelpHandbookAction->setText(QApplication::translate("MainWindow", "&KeePassX Handbook...", 0, QApplication::UnicodeUTF8));
        HideSearchResultsAction->setText(QApplication::translate("MainWindow", "Hide", 0, QApplication::UnicodeUTF8));
        EditAutoTypeAction->setText(QApplication::translate("MainWindow", "&Perform AutoType", 0, QApplication::UnicodeUTF8));
        ViewToolButtonSize16Action->setText(QApplication::translate("MainWindow", "&16x16", 0, QApplication::UnicodeUTF8));
        ViewToolButtonSize22Action->setText(QApplication::translate("MainWindow", "&22x22", 0, QApplication::UnicodeUTF8));
        ViewToolButtonSize28Action->setText(QApplication::translate("MainWindow", "2&8x28", 0, QApplication::UnicodeUTF8));
        FileNewAction->setText(QApplication::translate("MainWindow", "&New Database...", 0, QApplication::UnicodeUTF8));
        ExtrasPasswordGenAction->setText(QApplication::translate("MainWindow", "&Password Generator...", 0, QApplication::UnicodeUTF8));
        ViewColumnsGroupAction->setText(QApplication::translate("MainWindow", "&Group", 0, QApplication::UnicodeUTF8));
        ExtrasShowExpiredEntriesAction->setText(QApplication::translate("MainWindow", "Show &Expired Entries...", 0, QApplication::UnicodeUTF8));
        ExtrasTrashCanAction->setText(QApplication::translate("MainWindow", "Recycle Bin...", 0, QApplication::UnicodeUTF8));
        AddBookmarkAction->setText(QApplication::translate("MainWindow", "&Add Bookmark...", 0, QApplication::UnicodeUTF8));
        AddThisAsBookmarkAction->setText(QApplication::translate("MainWindow", "Bookmark &this Database...", 0, QApplication::UnicodeUTF8));
        EditCopyUrlAction->setText(QApplication::translate("MainWindow", "Copy URL to Clipboard", 0, QApplication::UnicodeUTF8));
        EditNewGroupAction->setText(QApplication::translate("MainWindow", "Add New Group...", 0, QApplication::UnicodeUTF8));
        EditGroupSortAction->setText(QApplication::translate("MainWindow", "Sort groups", 0, QApplication::UnicodeUTF8));
        ViewMinimizeAction->setText(QApplication::translate("MainWindow", "&Minimize Window", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = GroupView->headerItem();
        ___qtreewidgetitem->setText(0, QApplication::translate("MainWindow", "Groups", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "&Help", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "&File", 0, QApplication::UnicodeUTF8));
        menuExport->setTitle(QApplication::translate("MainWindow", "&Export to...", 0, QApplication::UnicodeUTF8));
        menuImport->setTitle(QApplication::translate("MainWindow", "&Import from...", 0, QApplication::UnicodeUTF8));
        menuBookmarks->setTitle(QApplication::translate("MainWindow", "&Bookmarks", 0, QApplication::UnicodeUTF8));
        menuEntries->setTitle(QApplication::translate("MainWindow", "&Entries", 0, QApplication::UnicodeUTF8));
        ViewMenu->setTitle(QApplication::translate("MainWindow", "&View", 0, QApplication::UnicodeUTF8));
        menuTool_Button_Sizes->setTitle(QApplication::translate("MainWindow", "Toolbar &Icon Size", 0, QApplication::UnicodeUTF8));
        menuColumns->setTitle(QApplication::translate("MainWindow", "&Columns", 0, QApplication::UnicodeUTF8));
        menuExtras->setTitle(QApplication::translate("MainWindow", "E&xtras", 0, QApplication::UnicodeUTF8));
        menuGroups->setTitle(QApplication::translate("MainWindow", "&Groups", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
