/********************************************************************************
** Form generated from reading UI file 'SettingsDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDLG_H
#define UI_SETTINGSDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "lib/ShortcutWidget.h"

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QHBoxLayout *horizontalLayout;
    QListWidget *listWidget;
    QStackedWidget *stackedWidget;
    QWidget *pageGeneral1;
    QVBoxLayout *verticalLayout;
    QCheckBox *CheckBox_ShowSysTrayIcon;
    QHBoxLayout *_16;
    QSpacerItem *spacerItem1;
    QCheckBox *CheckBox_MinimizeTray;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_3;
    QCheckBox *CheckBox_CloseToTray;
    QVBoxLayout *_18;
    QCheckBox *CheckBox_OpenLast;
    QHBoxLayout *_19;
    QSpacerItem *spacerItem2;
    QCheckBox *CheckBox_RememberLastKey;
    QHBoxLayout *_20;
    QSpacerItem *spacerItem3;
    QCheckBox *CheckBox_StartMinimized;
    QHBoxLayout *_21;
    QSpacerItem *spacerItem4;
    QCheckBox *CheckBox_StartLocked;
    QHBoxLayout *_22;
    QCheckBox *checkBox_SaveFileDlgHistory;
    QSpacerItem *spacerItem5;
    QPushButton *Button_ClearFileDlgHistory;
    QSpacerItem *spacerItem6;
    QCheckBox *checkBox_AskBeforeDelete;
    QSpacerItem *spacerItem7;
    QWidget *pageGeneral2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *CheckBox_Backup;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *spacerItem8;
    QCheckBox *CheckBox_BackupDelete;
    QSpinBox *SpinBox_BackupDeleteAfter;
    QLabel *label;
    QCheckBox *CheckBox_AutoSave;
    QCheckBox *CheckBox_AutoSaveChange;
    QSpacerItem *verticalSpacer;
    QWidget *pageAppearance;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox1;
    QVBoxLayout *_23;
    QGridLayout *_24;
    QLabel *textLabel2_2;
    QSpacerItem *spacerItem9;
    QPushButton *ButtonTextColor;
    QLabel *textLabel3;
    QLabel *pixmColor1;
    QLabel *pixmTextColor;
    QPushButton *ButtonColor2;
    QLabel *pixmColor2;
    QPushButton *ButtonColor1;
    QLabel *textLabel1_2;
    QSpacerItem *spacerItem10;
    QCheckBox *CheckBox_AlwaysOnTop;
    QCheckBox *CheckBox_AlternatingRowColors;
    QHBoxLayout *_25;
    QPushButton *Button_CustomizeEntryDetails;
    QSpacerItem *spacerItem11;
    QHBoxLayout *_26;
    QGridLayout *_27;
    QLabel *label_3;
    QRadioButton *Radio_GroupTreeRestore;
    QRadioButton *Radio_GroupTreeExpand;
    QRadioButton *Radio_GroupTreeDoNothing;
    QSpacerItem *spacerItem12;
    QSpacerItem *spacer_2;
    QWidget *pageLanguage;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_3;
    QListWidget *listSelectLanguage;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *labelLang;
    QLabel *labelAuthor;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;
    QWidget *pageSecurity;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_2;
    QVBoxLayout *_28;
    QCheckBox *CheckBox_ShowPasswords;
    QCheckBox *CheckBox_ShowPasswords_PasswordDlg;
    QHBoxLayout *_29;
    QLabel *textLabel1;
    QSpinBox *SpinBox_ClipboardTime;
    QLabel *textLabel2;
    QCheckBox *CheckBox_LockMinimize;
    QHBoxLayout *_30;
    QCheckBox *CheckBox_InactivityLock;
    QSpinBox *SpinBox_InacitivtyTime;
    QLabel *label_Inactivity;
    QSpacerItem *spacer_3;
    QWidget *pageAdvanced;
    QVBoxLayout *verticalLayout_5;
    QGroupBox *Box_BrowserCmd;
    QHBoxLayout *_31;
    QLineEdit *Edit_BrowserCmd;
    QPushButton *Button_BrowserCmdBrowse;
    QHBoxLayout *_32;
    QLabel *label_2;
    QLineEdit *Edit_MountDir;
    QPushButton *Button_MountDirBrowse;
    QCheckBox *CheckBox_SaveRelativePaths;
    QGroupBox *Box_AutoType;
    QGridLayout *_33;
    QSpinBox *SpinBox_AutoTypePreGap;
    QLabel *label_4;
    QSpacerItem *spacerItem13;
    QLabel *label_5;
    QSpinBox *SpinBox_AutoTypeKeyStrokeDelay;
    QHBoxLayout *_34;
    QLabel *Label_GlobalShortcut;
    ShortcutWidget *Edit_GlobalShortcut;
    QSpacerItem *spacerItem14;
    QCheckBox *CheckBox_EntryTitlesMatch;
    QSpacerItem *spacer_4;
    QWidget *pageFeatures;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_6;
    QCheckBox *CheckBox_FeatureBookmarks;
    QSpacerItem *spacer_5;
    QWidget *pageDesktop;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBox;
    QVBoxLayout *_35;
    QRadioButton *Radio_IntPlugin_None;
    QRadioButton *Radio_IntPlugin_Gnome;
    QRadioButton *Radio_IntPlugin_Kde;
    QHBoxLayout *_36;
    QLabel *Label_IntPlugin_Info;
    QSpacerItem *spacerItem15;
    QPushButton *IntPlugin_Button_Config;
    QSpacerItem *spacer_6;
    QDialogButtonBox *DialogButtons;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
        SettingsDialog->resize(667, 440);
        SettingsDialog->setModal(true);
        vboxLayout = new QVBoxLayout(SettingsDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        listWidget = new QListWidget(SettingsDialog);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        new QListWidgetItem(listWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listWidget->sizePolicy().hasHeightForWidth());
        listWidget->setSizePolicy(sizePolicy);
        listWidget->setMaximumSize(QSize(120, 16777215));
        QFont font;
        font.setPointSize(10);
        listWidget->setFont(font);
        listWidget->setStyleSheet(QString::fromUtf8("QListView {\n"
"	show-decoration-selected: 1;\n"
"}\n"
"\n"
"QListView::item {\n"
"	margin-top: 2px;\n"
"	margin-bottom: 2px;\n"
"}"));
        listWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

        horizontalLayout->addWidget(listWidget);

        stackedWidget = new QStackedWidget(SettingsDialog);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        pageGeneral1 = new QWidget();
        pageGeneral1->setObjectName(QString::fromUtf8("pageGeneral1"));
        verticalLayout = new QVBoxLayout(pageGeneral1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        CheckBox_ShowSysTrayIcon = new QCheckBox(pageGeneral1);
        CheckBox_ShowSysTrayIcon->setObjectName(QString::fromUtf8("CheckBox_ShowSysTrayIcon"));

        verticalLayout->addWidget(CheckBox_ShowSysTrayIcon);

        _16 = new QHBoxLayout();
        _16->setSpacing(6);
        _16->setObjectName(QString::fromUtf8("_16"));
        spacerItem1 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _16->addItem(spacerItem1);

        CheckBox_MinimizeTray = new QCheckBox(pageGeneral1);
        CheckBox_MinimizeTray->setObjectName(QString::fromUtf8("CheckBox_MinimizeTray"));
        CheckBox_MinimizeTray->setEnabled(false);

        _16->addWidget(CheckBox_MinimizeTray);


        verticalLayout->addLayout(_16);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_3 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_3);

        CheckBox_CloseToTray = new QCheckBox(pageGeneral1);
        CheckBox_CloseToTray->setObjectName(QString::fromUtf8("CheckBox_CloseToTray"));
        CheckBox_CloseToTray->setEnabled(false);

        horizontalLayout_6->addWidget(CheckBox_CloseToTray);


        verticalLayout->addLayout(horizontalLayout_6);

        _18 = new QVBoxLayout();
        _18->setSpacing(6);
        _18->setObjectName(QString::fromUtf8("_18"));
        CheckBox_OpenLast = new QCheckBox(pageGeneral1);
        CheckBox_OpenLast->setObjectName(QString::fromUtf8("CheckBox_OpenLast"));

        _18->addWidget(CheckBox_OpenLast);

        _19 = new QHBoxLayout();
        _19->setSpacing(6);
        _19->setObjectName(QString::fromUtf8("_19"));
        spacerItem2 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _19->addItem(spacerItem2);

        CheckBox_RememberLastKey = new QCheckBox(pageGeneral1);
        CheckBox_RememberLastKey->setObjectName(QString::fromUtf8("CheckBox_RememberLastKey"));
        CheckBox_RememberLastKey->setEnabled(false);

        _19->addWidget(CheckBox_RememberLastKey);


        _18->addLayout(_19);

        _20 = new QHBoxLayout();
        _20->setSpacing(6);
        _20->setObjectName(QString::fromUtf8("_20"));
        spacerItem3 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _20->addItem(spacerItem3);

        CheckBox_StartMinimized = new QCheckBox(pageGeneral1);
        CheckBox_StartMinimized->setObjectName(QString::fromUtf8("CheckBox_StartMinimized"));
        CheckBox_StartMinimized->setEnabled(false);

        _20->addWidget(CheckBox_StartMinimized);


        _18->addLayout(_20);

        _21 = new QHBoxLayout();
        _21->setSpacing(6);
        _21->setObjectName(QString::fromUtf8("_21"));
        spacerItem4 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _21->addItem(spacerItem4);

        CheckBox_StartLocked = new QCheckBox(pageGeneral1);
        CheckBox_StartLocked->setObjectName(QString::fromUtf8("CheckBox_StartLocked"));
        CheckBox_StartLocked->setEnabled(false);

        _21->addWidget(CheckBox_StartLocked);


        _18->addLayout(_21);


        verticalLayout->addLayout(_18);

        _22 = new QHBoxLayout();
        _22->setSpacing(6);
        _22->setObjectName(QString::fromUtf8("_22"));
        checkBox_SaveFileDlgHistory = new QCheckBox(pageGeneral1);
        checkBox_SaveFileDlgHistory->setObjectName(QString::fromUtf8("checkBox_SaveFileDlgHistory"));

        _22->addWidget(checkBox_SaveFileDlgHistory);

        spacerItem5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _22->addItem(spacerItem5);

        Button_ClearFileDlgHistory = new QPushButton(pageGeneral1);
        Button_ClearFileDlgHistory->setObjectName(QString::fromUtf8("Button_ClearFileDlgHistory"));

        _22->addWidget(Button_ClearFileDlgHistory);

        spacerItem6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _22->addItem(spacerItem6);


        verticalLayout->addLayout(_22);

        checkBox_AskBeforeDelete = new QCheckBox(pageGeneral1);
        checkBox_AskBeforeDelete->setObjectName(QString::fromUtf8("checkBox_AskBeforeDelete"));

        verticalLayout->addWidget(checkBox_AskBeforeDelete);

        spacerItem7 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(spacerItem7);

        stackedWidget->addWidget(pageGeneral1);
        pageGeneral2 = new QWidget();
        pageGeneral2->setObjectName(QString::fromUtf8("pageGeneral2"));
        verticalLayout_2 = new QVBoxLayout(pageGeneral2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        CheckBox_Backup = new QCheckBox(pageGeneral2);
        CheckBox_Backup->setObjectName(QString::fromUtf8("CheckBox_Backup"));

        verticalLayout_2->addWidget(CheckBox_Backup);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        spacerItem8 = new QSpacerItem(25, 10, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(spacerItem8);

        CheckBox_BackupDelete = new QCheckBox(pageGeneral2);
        CheckBox_BackupDelete->setObjectName(QString::fromUtf8("CheckBox_BackupDelete"));
        CheckBox_BackupDelete->setEnabled(false);

        horizontalLayout_2->addWidget(CheckBox_BackupDelete);

        SpinBox_BackupDeleteAfter = new QSpinBox(pageGeneral2);
        SpinBox_BackupDeleteAfter->setObjectName(QString::fromUtf8("SpinBox_BackupDeleteAfter"));
        SpinBox_BackupDeleteAfter->setEnabled(false);
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(SpinBox_BackupDeleteAfter->sizePolicy().hasHeightForWidth());
        SpinBox_BackupDeleteAfter->setSizePolicy(sizePolicy1);
        SpinBox_BackupDeleteAfter->setMinimum(1);
        SpinBox_BackupDeleteAfter->setMaximum(999);

        horizontalLayout_2->addWidget(SpinBox_BackupDeleteAfter);

        label = new QLabel(pageGeneral2);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_2->addWidget(label);


        verticalLayout_2->addLayout(horizontalLayout_2);

        CheckBox_AutoSave = new QCheckBox(pageGeneral2);
        CheckBox_AutoSave->setObjectName(QString::fromUtf8("CheckBox_AutoSave"));

        verticalLayout_2->addWidget(CheckBox_AutoSave);

        CheckBox_AutoSaveChange = new QCheckBox(pageGeneral2);
        CheckBox_AutoSaveChange->setObjectName(QString::fromUtf8("CheckBox_AutoSaveChange"));

        verticalLayout_2->addWidget(CheckBox_AutoSaveChange);

        verticalSpacer = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);

        stackedWidget->addWidget(pageGeneral2);
        pageAppearance = new QWidget();
        pageAppearance->setObjectName(QString::fromUtf8("pageAppearance"));
        verticalLayout_3 = new QVBoxLayout(pageAppearance);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox1 = new QGroupBox(pageAppearance);
        groupBox1->setObjectName(QString::fromUtf8("groupBox1"));
        _23 = new QVBoxLayout(groupBox1);
        _23->setSpacing(6);
        _23->setContentsMargins(11, 11, 11, 11);
        _23->setObjectName(QString::fromUtf8("_23"));
        _24 = new QGridLayout();
        _24->setSpacing(6);
        _24->setObjectName(QString::fromUtf8("_24"));
        textLabel2_2 = new QLabel(groupBox1);
        textLabel2_2->setObjectName(QString::fromUtf8("textLabel2_2"));

        _24->addWidget(textLabel2_2, 1, 0, 1, 1);

        spacerItem9 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        _24->addItem(spacerItem9, 0, 3, 1, 1);

        ButtonTextColor = new QPushButton(groupBox1);
        ButtonTextColor->setObjectName(QString::fromUtf8("ButtonTextColor"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(ButtonTextColor->sizePolicy().hasHeightForWidth());
        ButtonTextColor->setSizePolicy(sizePolicy2);
        ButtonTextColor->setMinimumSize(QSize(0, 23));
        ButtonTextColor->setMaximumSize(QSize(16777215, 23));

        _24->addWidget(ButtonTextColor, 1, 2, 1, 1);

        textLabel3 = new QLabel(groupBox1);
        textLabel3->setObjectName(QString::fromUtf8("textLabel3"));
        QSizePolicy sizePolicy3(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(textLabel3->sizePolicy().hasHeightForWidth());
        textLabel3->setSizePolicy(sizePolicy3);

        _24->addWidget(textLabel3, 0, 4, 1, 1);

        pixmColor1 = new QLabel(groupBox1);
        pixmColor1->setObjectName(QString::fromUtf8("pixmColor1"));
        sizePolicy2.setHeightForWidth(pixmColor1->sizePolicy().hasHeightForWidth());
        pixmColor1->setSizePolicy(sizePolicy2);
        pixmColor1->setMinimumSize(QSize(23, 23));
        pixmColor1->setMaximumSize(QSize(23, 23));

        _24->addWidget(pixmColor1, 0, 1, 1, 1);

        pixmTextColor = new QLabel(groupBox1);
        pixmTextColor->setObjectName(QString::fromUtf8("pixmTextColor"));
        sizePolicy2.setHeightForWidth(pixmTextColor->sizePolicy().hasHeightForWidth());
        pixmTextColor->setSizePolicy(sizePolicy2);
        pixmTextColor->setMinimumSize(QSize(23, 23));
        pixmTextColor->setMaximumSize(QSize(23, 23));
        pixmTextColor->setScaledContents(true);

        _24->addWidget(pixmTextColor, 1, 1, 1, 1);

        ButtonColor2 = new QPushButton(groupBox1);
        ButtonColor2->setObjectName(QString::fromUtf8("ButtonColor2"));
        sizePolicy2.setHeightForWidth(ButtonColor2->sizePolicy().hasHeightForWidth());
        ButtonColor2->setSizePolicy(sizePolicy2);
        ButtonColor2->setMinimumSize(QSize(0, 23));
        ButtonColor2->setMaximumSize(QSize(16777215, 23));

        _24->addWidget(ButtonColor2, 0, 6, 1, 1);

        pixmColor2 = new QLabel(groupBox1);
        pixmColor2->setObjectName(QString::fromUtf8("pixmColor2"));
        sizePolicy2.setHeightForWidth(pixmColor2->sizePolicy().hasHeightForWidth());
        pixmColor2->setSizePolicy(sizePolicy2);
        pixmColor2->setMinimumSize(QSize(23, 23));
        pixmColor2->setMaximumSize(QSize(23, 23));
        pixmColor2->setScaledContents(true);

        _24->addWidget(pixmColor2, 0, 5, 1, 1);

        ButtonColor1 = new QPushButton(groupBox1);
        ButtonColor1->setObjectName(QString::fromUtf8("ButtonColor1"));
        sizePolicy2.setHeightForWidth(ButtonColor1->sizePolicy().hasHeightForWidth());
        ButtonColor1->setSizePolicy(sizePolicy2);
        ButtonColor1->setMinimumSize(QSize(0, 23));
        ButtonColor1->setMaximumSize(QSize(16777215, 23));

        _24->addWidget(ButtonColor1, 0, 2, 1, 1);

        textLabel1_2 = new QLabel(groupBox1);
        textLabel1_2->setObjectName(QString::fromUtf8("textLabel1_2"));
        sizePolicy3.setHeightForWidth(textLabel1_2->sizePolicy().hasHeightForWidth());
        textLabel1_2->setSizePolicy(sizePolicy3);

        _24->addWidget(textLabel1_2, 0, 0, 1, 1);

        spacerItem10 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        _24->addItem(spacerItem10, 0, 7, 1, 1);


        _23->addLayout(_24);


        verticalLayout_3->addWidget(groupBox1);

        CheckBox_AlwaysOnTop = new QCheckBox(pageAppearance);
        CheckBox_AlwaysOnTop->setObjectName(QString::fromUtf8("CheckBox_AlwaysOnTop"));

        verticalLayout_3->addWidget(CheckBox_AlwaysOnTop);

        CheckBox_AlternatingRowColors = new QCheckBox(pageAppearance);
        CheckBox_AlternatingRowColors->setObjectName(QString::fromUtf8("CheckBox_AlternatingRowColors"));

        verticalLayout_3->addWidget(CheckBox_AlternatingRowColors);

        _25 = new QHBoxLayout();
        _25->setSpacing(6);
        _25->setObjectName(QString::fromUtf8("_25"));
        Button_CustomizeEntryDetails = new QPushButton(pageAppearance);
        Button_CustomizeEntryDetails->setObjectName(QString::fromUtf8("Button_CustomizeEntryDetails"));

        _25->addWidget(Button_CustomizeEntryDetails);

        spacerItem11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _25->addItem(spacerItem11);


        verticalLayout_3->addLayout(_25);

        _26 = new QHBoxLayout();
        _26->setSpacing(6);
        _26->setObjectName(QString::fromUtf8("_26"));
        _27 = new QGridLayout();
        _27->setSpacing(6);
        _27->setObjectName(QString::fromUtf8("_27"));
        label_3 = new QLabel(pageAppearance);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        _27->addWidget(label_3, 0, 0, 1, 1);

        Radio_GroupTreeRestore = new QRadioButton(pageAppearance);
        Radio_GroupTreeRestore->setObjectName(QString::fromUtf8("Radio_GroupTreeRestore"));

        _27->addWidget(Radio_GroupTreeRestore, 0, 1, 1, 1);

        Radio_GroupTreeExpand = new QRadioButton(pageAppearance);
        Radio_GroupTreeExpand->setObjectName(QString::fromUtf8("Radio_GroupTreeExpand"));

        _27->addWidget(Radio_GroupTreeExpand, 1, 1, 1, 1);

        Radio_GroupTreeDoNothing = new QRadioButton(pageAppearance);
        Radio_GroupTreeDoNothing->setObjectName(QString::fromUtf8("Radio_GroupTreeDoNothing"));

        _27->addWidget(Radio_GroupTreeDoNothing, 2, 1, 1, 1);


        _26->addLayout(_27);

        spacerItem12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _26->addItem(spacerItem12);


        verticalLayout_3->addLayout(_26);

        spacer_2 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(spacer_2);

        stackedWidget->addWidget(pageAppearance);
        pageLanguage = new QWidget();
        pageLanguage->setObjectName(QString::fromUtf8("pageLanguage"));
        verticalLayout_8 = new QVBoxLayout(pageLanguage);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        listSelectLanguage = new QListWidget(pageLanguage);
        listSelectLanguage->setObjectName(QString::fromUtf8("listSelectLanguage"));
        listSelectLanguage->setSelectionBehavior(QAbstractItemView::SelectRows);

        horizontalLayout_3->addWidget(listSelectLanguage);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_8->addLayout(horizontalLayout_3);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_7 = new QLabel(pageLanguage);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout->addWidget(label_7, 0, 0, 1, 1);

        label_8 = new QLabel(pageLanguage);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 1, 0, 1, 1);

        labelLang = new QLabel(pageLanguage);
        labelLang->setObjectName(QString::fromUtf8("labelLang"));

        gridLayout->addWidget(labelLang, 0, 1, 1, 1);

        labelAuthor = new QLabel(pageLanguage);
        labelAuthor->setObjectName(QString::fromUtf8("labelAuthor"));

        gridLayout->addWidget(labelAuthor, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);


        verticalLayout_8->addLayout(gridLayout);

        verticalSpacer_2 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_8->addItem(verticalSpacer_2);

        stackedWidget->addWidget(pageLanguage);
        pageSecurity = new QWidget();
        pageSecurity->setObjectName(QString::fromUtf8("pageSecurity"));
        verticalLayout_4 = new QVBoxLayout(pageSecurity);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox_2 = new QGroupBox(pageSecurity);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(groupBox_2->sizePolicy().hasHeightForWidth());
        groupBox_2->setSizePolicy(sizePolicy4);
        _28 = new QVBoxLayout(groupBox_2);
        _28->setSpacing(0);
        _28->setContentsMargins(11, 11, 11, 11);
        _28->setObjectName(QString::fromUtf8("_28"));
        CheckBox_ShowPasswords = new QCheckBox(groupBox_2);
        CheckBox_ShowPasswords->setObjectName(QString::fromUtf8("CheckBox_ShowPasswords"));

        _28->addWidget(CheckBox_ShowPasswords);

        CheckBox_ShowPasswords_PasswordDlg = new QCheckBox(groupBox_2);
        CheckBox_ShowPasswords_PasswordDlg->setObjectName(QString::fromUtf8("CheckBox_ShowPasswords_PasswordDlg"));

        _28->addWidget(CheckBox_ShowPasswords_PasswordDlg);


        verticalLayout_4->addWidget(groupBox_2);

        _29 = new QHBoxLayout();
        _29->setSpacing(6);
        _29->setObjectName(QString::fromUtf8("_29"));
        textLabel1 = new QLabel(pageSecurity);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        sizePolicy3.setHeightForWidth(textLabel1->sizePolicy().hasHeightForWidth());
        textLabel1->setSizePolicy(sizePolicy3);

        _29->addWidget(textLabel1);

        SpinBox_ClipboardTime = new QSpinBox(pageSecurity);
        SpinBox_ClipboardTime->setObjectName(QString::fromUtf8("SpinBox_ClipboardTime"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(SpinBox_ClipboardTime->sizePolicy().hasHeightForWidth());
        SpinBox_ClipboardTime->setSizePolicy(sizePolicy5);
        SpinBox_ClipboardTime->setMaximumSize(QSize(80, 16777215));
        SpinBox_ClipboardTime->setMaximum(50000);
        SpinBox_ClipboardTime->setValue(2);

        _29->addWidget(SpinBox_ClipboardTime);

        textLabel2 = new QLabel(pageSecurity);
        textLabel2->setObjectName(QString::fromUtf8("textLabel2"));

        _29->addWidget(textLabel2);


        verticalLayout_4->addLayout(_29);

        CheckBox_LockMinimize = new QCheckBox(pageSecurity);
        CheckBox_LockMinimize->setObjectName(QString::fromUtf8("CheckBox_LockMinimize"));

        verticalLayout_4->addWidget(CheckBox_LockMinimize);

        _30 = new QHBoxLayout();
        _30->setSpacing(6);
        _30->setObjectName(QString::fromUtf8("_30"));
        CheckBox_InactivityLock = new QCheckBox(pageSecurity);
        CheckBox_InactivityLock->setObjectName(QString::fromUtf8("CheckBox_InactivityLock"));

        _30->addWidget(CheckBox_InactivityLock);

        SpinBox_InacitivtyTime = new QSpinBox(pageSecurity);
        SpinBox_InacitivtyTime->setObjectName(QString::fromUtf8("SpinBox_InacitivtyTime"));
        SpinBox_InacitivtyTime->setEnabled(false);
        sizePolicy5.setHeightForWidth(SpinBox_InacitivtyTime->sizePolicy().hasHeightForWidth());
        SpinBox_InacitivtyTime->setSizePolicy(sizePolicy5);
        SpinBox_InacitivtyTime->setMaximumSize(QSize(80, 16777215));
        SpinBox_InacitivtyTime->setMaximum(50000);

        _30->addWidget(SpinBox_InacitivtyTime);

        label_Inactivity = new QLabel(pageSecurity);
        label_Inactivity->setObjectName(QString::fromUtf8("label_Inactivity"));

        _30->addWidget(label_Inactivity);


        verticalLayout_4->addLayout(_30);

        spacer_3 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(spacer_3);

        stackedWidget->addWidget(pageSecurity);
        pageAdvanced = new QWidget();
        pageAdvanced->setObjectName(QString::fromUtf8("pageAdvanced"));
        verticalLayout_5 = new QVBoxLayout(pageAdvanced);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        Box_BrowserCmd = new QGroupBox(pageAdvanced);
        Box_BrowserCmd->setObjectName(QString::fromUtf8("Box_BrowserCmd"));
        Box_BrowserCmd->setCheckable(true);
        _31 = new QHBoxLayout(Box_BrowserCmd);
        _31->setSpacing(6);
        _31->setContentsMargins(11, 11, 11, 11);
        _31->setObjectName(QString::fromUtf8("_31"));
        Edit_BrowserCmd = new QLineEdit(Box_BrowserCmd);
        Edit_BrowserCmd->setObjectName(QString::fromUtf8("Edit_BrowserCmd"));

        _31->addWidget(Edit_BrowserCmd);

        Button_BrowserCmdBrowse = new QPushButton(Box_BrowserCmd);
        Button_BrowserCmdBrowse->setObjectName(QString::fromUtf8("Button_BrowserCmdBrowse"));

        _31->addWidget(Button_BrowserCmdBrowse);


        verticalLayout_5->addWidget(Box_BrowserCmd);

        _32 = new QHBoxLayout();
        _32->setSpacing(6);
        _32->setObjectName(QString::fromUtf8("_32"));
        label_2 = new QLabel(pageAdvanced);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        _32->addWidget(label_2);

        Edit_MountDir = new QLineEdit(pageAdvanced);
        Edit_MountDir->setObjectName(QString::fromUtf8("Edit_MountDir"));

        _32->addWidget(Edit_MountDir);

        Button_MountDirBrowse = new QPushButton(pageAdvanced);
        Button_MountDirBrowse->setObjectName(QString::fromUtf8("Button_MountDirBrowse"));

        _32->addWidget(Button_MountDirBrowse);


        verticalLayout_5->addLayout(_32);

        CheckBox_SaveRelativePaths = new QCheckBox(pageAdvanced);
        CheckBox_SaveRelativePaths->setObjectName(QString::fromUtf8("CheckBox_SaveRelativePaths"));

        verticalLayout_5->addWidget(CheckBox_SaveRelativePaths);

        Box_AutoType = new QGroupBox(pageAdvanced);
        Box_AutoType->setObjectName(QString::fromUtf8("Box_AutoType"));
        _33 = new QGridLayout(Box_AutoType);
        _33->setSpacing(6);
        _33->setContentsMargins(11, 11, 11, 11);
        _33->setObjectName(QString::fromUtf8("_33"));
        SpinBox_AutoTypePreGap = new QSpinBox(Box_AutoType);
        SpinBox_AutoTypePreGap->setObjectName(QString::fromUtf8("SpinBox_AutoTypePreGap"));
        SpinBox_AutoTypePreGap->setMaximum(10000);

        _33->addWidget(SpinBox_AutoTypePreGap, 0, 1, 1, 1);

        label_4 = new QLabel(Box_AutoType);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        _33->addWidget(label_4, 0, 0, 1, 1);

        spacerItem13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _33->addItem(spacerItem13, 0, 2, 1, 1);

        label_5 = new QLabel(Box_AutoType);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        _33->addWidget(label_5, 1, 0, 1, 1);

        SpinBox_AutoTypeKeyStrokeDelay = new QSpinBox(Box_AutoType);
        SpinBox_AutoTypeKeyStrokeDelay->setObjectName(QString::fromUtf8("SpinBox_AutoTypeKeyStrokeDelay"));
        SpinBox_AutoTypeKeyStrokeDelay->setMaximum(10000);

        _33->addWidget(SpinBox_AutoTypeKeyStrokeDelay, 1, 1, 1, 1);


        verticalLayout_5->addWidget(Box_AutoType);

        _34 = new QHBoxLayout();
        _34->setSpacing(6);
        _34->setObjectName(QString::fromUtf8("_34"));
        Label_GlobalShortcut = new QLabel(pageAdvanced);
        Label_GlobalShortcut->setObjectName(QString::fromUtf8("Label_GlobalShortcut"));

        _34->addWidget(Label_GlobalShortcut);

        Edit_GlobalShortcut = new ShortcutWidget(pageAdvanced);
        Edit_GlobalShortcut->setObjectName(QString::fromUtf8("Edit_GlobalShortcut"));
        Edit_GlobalShortcut->setReadOnly(true);

        _34->addWidget(Edit_GlobalShortcut);

        spacerItem14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _34->addItem(spacerItem14);


        verticalLayout_5->addLayout(_34);

        CheckBox_EntryTitlesMatch = new QCheckBox(pageAdvanced);
        CheckBox_EntryTitlesMatch->setObjectName(QString::fromUtf8("CheckBox_EntryTitlesMatch"));

        verticalLayout_5->addWidget(CheckBox_EntryTitlesMatch);

        spacer_4 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(spacer_4);

        stackedWidget->addWidget(pageAdvanced);
        pageFeatures = new QWidget();
        pageFeatures->setObjectName(QString::fromUtf8("pageFeatures"));
        verticalLayout_6 = new QVBoxLayout(pageFeatures);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_6 = new QLabel(pageFeatures);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setWordWrap(true);

        verticalLayout_6->addWidget(label_6);

        CheckBox_FeatureBookmarks = new QCheckBox(pageFeatures);
        CheckBox_FeatureBookmarks->setObjectName(QString::fromUtf8("CheckBox_FeatureBookmarks"));

        verticalLayout_6->addWidget(CheckBox_FeatureBookmarks);

        spacer_5 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(spacer_5);

        stackedWidget->addWidget(pageFeatures);
        pageDesktop = new QWidget();
        pageDesktop->setObjectName(QString::fromUtf8("pageDesktop"));
        verticalLayout_7 = new QVBoxLayout(pageDesktop);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        groupBox = new QGroupBox(pageDesktop);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        sizePolicy4.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy4);
        _35 = new QVBoxLayout(groupBox);
        _35->setSpacing(6);
        _35->setContentsMargins(11, 11, 11, 11);
        _35->setObjectName(QString::fromUtf8("_35"));
        Radio_IntPlugin_None = new QRadioButton(groupBox);
        Radio_IntPlugin_None->setObjectName(QString::fromUtf8("Radio_IntPlugin_None"));

        _35->addWidget(Radio_IntPlugin_None);

        Radio_IntPlugin_Gnome = new QRadioButton(groupBox);
        Radio_IntPlugin_Gnome->setObjectName(QString::fromUtf8("Radio_IntPlugin_Gnome"));

        _35->addWidget(Radio_IntPlugin_Gnome);

        Radio_IntPlugin_Kde = new QRadioButton(groupBox);
        Radio_IntPlugin_Kde->setObjectName(QString::fromUtf8("Radio_IntPlugin_Kde"));

        _35->addWidget(Radio_IntPlugin_Kde);


        verticalLayout_7->addWidget(groupBox);

        _36 = new QHBoxLayout();
        _36->setSpacing(6);
        _36->setObjectName(QString::fromUtf8("_36"));
        Label_IntPlugin_Info = new QLabel(pageDesktop);
        Label_IntPlugin_Info->setObjectName(QString::fromUtf8("Label_IntPlugin_Info"));

        _36->addWidget(Label_IntPlugin_Info);

        spacerItem15 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        _36->addItem(spacerItem15);

        IntPlugin_Button_Config = new QPushButton(pageDesktop);
        IntPlugin_Button_Config->setObjectName(QString::fromUtf8("IntPlugin_Button_Config"));

        _36->addWidget(IntPlugin_Button_Config);


        verticalLayout_7->addLayout(_36);

        spacer_6 = new QSpacerItem(1, 1, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(spacer_6);

        stackedWidget->addWidget(pageDesktop);

        horizontalLayout->addWidget(stackedWidget);


        vboxLayout->addLayout(horizontalLayout);

        DialogButtons = new QDialogButtonBox(SettingsDialog);
        DialogButtons->setObjectName(QString::fromUtf8("DialogButtons"));
        DialogButtons->setOrientation(Qt::Horizontal);
        DialogButtons->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(DialogButtons);

        QWidget::setTabOrder(listWidget, CheckBox_ShowSysTrayIcon);
        QWidget::setTabOrder(CheckBox_ShowSysTrayIcon, CheckBox_MinimizeTray);
        QWidget::setTabOrder(CheckBox_MinimizeTray, CheckBox_OpenLast);
        QWidget::setTabOrder(CheckBox_OpenLast, CheckBox_RememberLastKey);
        QWidget::setTabOrder(CheckBox_RememberLastKey, CheckBox_StartMinimized);
        QWidget::setTabOrder(CheckBox_StartMinimized, CheckBox_StartLocked);
        QWidget::setTabOrder(CheckBox_StartLocked, checkBox_SaveFileDlgHistory);
        QWidget::setTabOrder(checkBox_SaveFileDlgHistory, Button_ClearFileDlgHistory);
        QWidget::setTabOrder(Button_ClearFileDlgHistory, checkBox_AskBeforeDelete);
        QWidget::setTabOrder(checkBox_AskBeforeDelete, CheckBox_Backup);
        QWidget::setTabOrder(CheckBox_Backup, CheckBox_BackupDelete);
        QWidget::setTabOrder(CheckBox_BackupDelete, SpinBox_BackupDeleteAfter);
        QWidget::setTabOrder(SpinBox_BackupDeleteAfter, CheckBox_AutoSave);
        QWidget::setTabOrder(CheckBox_AutoSave, CheckBox_AutoSaveChange);
        QWidget::setTabOrder(CheckBox_AutoSaveChange, ButtonColor1);
        QWidget::setTabOrder(ButtonColor1, ButtonColor2);
        QWidget::setTabOrder(ButtonColor2, ButtonTextColor);
        QWidget::setTabOrder(ButtonTextColor, CheckBox_AlternatingRowColors);
        QWidget::setTabOrder(CheckBox_AlternatingRowColors, Button_CustomizeEntryDetails);
        QWidget::setTabOrder(Button_CustomizeEntryDetails, Radio_GroupTreeRestore);
        QWidget::setTabOrder(Radio_GroupTreeRestore, Radio_GroupTreeExpand);
        QWidget::setTabOrder(Radio_GroupTreeExpand, Radio_GroupTreeDoNothing);
        QWidget::setTabOrder(Radio_GroupTreeDoNothing, listSelectLanguage);
        QWidget::setTabOrder(listSelectLanguage, CheckBox_ShowPasswords);
        QWidget::setTabOrder(CheckBox_ShowPasswords, CheckBox_ShowPasswords_PasswordDlg);
        QWidget::setTabOrder(CheckBox_ShowPasswords_PasswordDlg, SpinBox_ClipboardTime);
        QWidget::setTabOrder(SpinBox_ClipboardTime, CheckBox_LockMinimize);
        QWidget::setTabOrder(CheckBox_LockMinimize, CheckBox_InactivityLock);
        QWidget::setTabOrder(CheckBox_InactivityLock, SpinBox_InacitivtyTime);
        QWidget::setTabOrder(SpinBox_InacitivtyTime, Box_BrowserCmd);
        QWidget::setTabOrder(Box_BrowserCmd, Edit_BrowserCmd);
        QWidget::setTabOrder(Edit_BrowserCmd, Button_BrowserCmdBrowse);
        QWidget::setTabOrder(Button_BrowserCmdBrowse, Edit_MountDir);
        QWidget::setTabOrder(Edit_MountDir, Button_MountDirBrowse);
        QWidget::setTabOrder(Button_MountDirBrowse, CheckBox_SaveRelativePaths);
        QWidget::setTabOrder(CheckBox_SaveRelativePaths, SpinBox_AutoTypePreGap);
        QWidget::setTabOrder(SpinBox_AutoTypePreGap, SpinBox_AutoTypeKeyStrokeDelay);
        QWidget::setTabOrder(SpinBox_AutoTypeKeyStrokeDelay, Edit_GlobalShortcut);
        QWidget::setTabOrder(Edit_GlobalShortcut, CheckBox_EntryTitlesMatch);
        QWidget::setTabOrder(CheckBox_EntryTitlesMatch, CheckBox_FeatureBookmarks);
        QWidget::setTabOrder(CheckBox_FeatureBookmarks, Radio_IntPlugin_None);
        QWidget::setTabOrder(Radio_IntPlugin_None, Radio_IntPlugin_Gnome);
        QWidget::setTabOrder(Radio_IntPlugin_Gnome, Radio_IntPlugin_Kde);
        QWidget::setTabOrder(Radio_IntPlugin_Kde, IntPlugin_Button_Config);
        QWidget::setTabOrder(IntPlugin_Button_Config, DialogButtons);

        retranslateUi(SettingsDialog);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Settings", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listWidget->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("SettingsDialog", "General (1)", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem1 = listWidget->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("SettingsDialog", "General (2)", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem2 = listWidget->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("SettingsDialog", "Appearance", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem3 = listWidget->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("SettingsDialog", "Language", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem4 = listWidget->item(4);
        ___qlistwidgetitem4->setText(QApplication::translate("SettingsDialog", "Security", 0, QApplication::UnicodeUTF8));
        QListWidgetItem *___qlistwidgetitem5 = listWidget->item(5);
        ___qlistwidgetitem5->setText(QApplication::translate("SettingsDialog", "Advanced", 0, QApplication::UnicodeUTF8));
        listWidget->setSortingEnabled(__sortingEnabled);

        CheckBox_ShowSysTrayIcon->setText(QApplication::translate("SettingsDialog", "Show system tray icon", 0, QApplication::UnicodeUTF8));
        CheckBox_MinimizeTray->setText(QApplication::translate("SettingsDialog", "Minimize to tray instead of taskbar", 0, QApplication::UnicodeUTF8));
        CheckBox_CloseToTray->setText(QApplication::translate("SettingsDialog", "Minimize to tray when clicking the main window's close button", 0, QApplication::UnicodeUTF8));
        CheckBox_OpenLast->setText(QApplication::translate("SettingsDialog", "Remember last opened file", 0, QApplication::UnicodeUTF8));
        CheckBox_RememberLastKey->setText(QApplication::translate("SettingsDialog", "Remember last key type and location", 0, QApplication::UnicodeUTF8));
        CheckBox_StartMinimized->setText(QApplication::translate("SettingsDialog", "Start minimized", 0, QApplication::UnicodeUTF8));
        CheckBox_StartLocked->setText(QApplication::translate("SettingsDialog", "Start locked", 0, QApplication::UnicodeUTF8));
        checkBox_SaveFileDlgHistory->setText(QApplication::translate("SettingsDialog", "Save recent directories of file dialogs", 0, QApplication::UnicodeUTF8));
        Button_ClearFileDlgHistory->setText(QApplication::translate("SettingsDialog", "Clear History Now", 0, QApplication::UnicodeUTF8));
        checkBox_AskBeforeDelete->setText(QApplication::translate("SettingsDialog", "Always ask before deleting entries or groups", 0, QApplication::UnicodeUTF8));
        CheckBox_Backup->setText(QApplication::translate("SettingsDialog", "Save backups of modified entries into the 'Backup' group", 0, QApplication::UnicodeUTF8));
        CheckBox_BackupDelete->setText(QApplication::translate("SettingsDialog", "Delete backup entries older than:", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SettingsDialog", "days", 0, QApplication::UnicodeUTF8));
        CheckBox_AutoSave->setText(QApplication::translate("SettingsDialog", "Automatically save database on exit and workspace locking", 0, QApplication::UnicodeUTF8));
        CheckBox_AutoSaveChange->setText(QApplication::translate("SettingsDialog", "Automatically save database after every change", 0, QApplication::UnicodeUTF8));
        groupBox1->setTitle(QApplication::translate("SettingsDialog", "Banner Color", 0, QApplication::UnicodeUTF8));
        textLabel2_2->setText(QApplication::translate("SettingsDialog", "Text Color:", 0, QApplication::UnicodeUTF8));
        ButtonTextColor->setText(QApplication::translate("SettingsDialog", "Change...", 0, QApplication::UnicodeUTF8));
        textLabel3->setText(QApplication::translate("SettingsDialog", "Color 2:", 0, QApplication::UnicodeUTF8));
        pixmColor1->setText(QString());
        ButtonColor2->setText(QApplication::translate("SettingsDialog", "Change...", 0, QApplication::UnicodeUTF8));
        ButtonColor1->setText(QApplication::translate("SettingsDialog", "C&hange...", 0, QApplication::UnicodeUTF8));
        textLabel1_2->setText(QApplication::translate("SettingsDialog", "Color 1:", 0, QApplication::UnicodeUTF8));
        CheckBox_AlwaysOnTop->setText(QApplication::translate("SettingsDialog", "Show window always on top", 0, QApplication::UnicodeUTF8));
        CheckBox_AlternatingRowColors->setText(QApplication::translate("SettingsDialog", "Alternating Row Colors", 0, QApplication::UnicodeUTF8));
        Button_CustomizeEntryDetails->setText(QApplication::translate("SettingsDialog", "Customize Entry Detail View...", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SettingsDialog", "Group tree at start-up:", 0, QApplication::UnicodeUTF8));
        Radio_GroupTreeRestore->setText(QApplication::translate("SettingsDialog", "Restore last state", 0, QApplication::UnicodeUTF8));
        Radio_GroupTreeExpand->setText(QApplication::translate("SettingsDialog", "Expand all items", 0, QApplication::UnicodeUTF8));
        Radio_GroupTreeDoNothing->setText(QApplication::translate("SettingsDialog", "Do not expand any item", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("SettingsDialog", "Language:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("SettingsDialog", "Author:", 0, QApplication::UnicodeUTF8));
        labelLang->setText(QString());
        labelAuthor->setText(QString());
        groupBox_2->setTitle(QApplication::translate("SettingsDialog", "Show plain text passwords in:", 0, QApplication::UnicodeUTF8));
        CheckBox_ShowPasswords->setText(QApplication::translate("SettingsDialog", "Edit Entry Dialog", 0, QApplication::UnicodeUTF8));
        CheckBox_ShowPasswords_PasswordDlg->setText(QApplication::translate("SettingsDialog", "Database Key Dialog", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("SettingsDialog", "Clear clipboard after:", 0, QApplication::UnicodeUTF8));
        textLabel2->setText(QApplication::translate("SettingsDialog", "seconds", 0, QApplication::UnicodeUTF8));
        CheckBox_LockMinimize->setText(QApplication::translate("SettingsDialog", "Lock workspace when minimizing the main window", 0, QApplication::UnicodeUTF8));
        CheckBox_InactivityLock->setText(QApplication::translate("SettingsDialog", "Lock database after inactivity of", 0, QApplication::UnicodeUTF8));
        label_Inactivity->setText(QApplication::translate("SettingsDialog", "seconds", 0, QApplication::UnicodeUTF8));
        Box_BrowserCmd->setTitle(QApplication::translate("SettingsDialog", "Custom Browser Command", 0, QApplication::UnicodeUTF8));
        Button_BrowserCmdBrowse->setText(QApplication::translate("SettingsDialog", "Browse", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SettingsDialog", "Media Root:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        Edit_MountDir->setWhatsThis(QApplication::translate("SettingsDialog", "The directory where storage devices like CDs and memory sticks are normally mounted.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        Button_MountDirBrowse->setText(QApplication::translate("SettingsDialog", "Browse...", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        CheckBox_SaveRelativePaths->setWhatsThis(QApplication::translate("SettingsDialog", "Enable this if you want to use your bookmarks and the last opened file independet from their absolute paths. This is especially useful when using KeePassX portably and therefore with changing mount points in the file system.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        CheckBox_SaveRelativePaths->setText(QApplication::translate("SettingsDialog", "Save relative paths (bookmarks and last file)", 0, QApplication::UnicodeUTF8));
        Box_AutoType->setTitle(QApplication::translate("SettingsDialog", "Auto-Type Fine Tuning", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        SpinBox_AutoTypePreGap->setWhatsThis(QApplication::translate("SettingsDialog", "Time between the activation of an auto-type action by the user and the first simulated key stroke.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        SpinBox_AutoTypePreGap->setSuffix(QApplication::translate("SettingsDialog", "ms", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SettingsDialog", "Pre-Gap:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SettingsDialog", "Key Stroke Delay:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        SpinBox_AutoTypeKeyStrokeDelay->setWhatsThis(QApplication::translate("SettingsDialog", "Delay between two simulated key strokes. Increase this if Auto-Type is randomly skipping characters.", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        SpinBox_AutoTypeKeyStrokeDelay->setSuffix(QApplication::translate("SettingsDialog", "ms", 0, QApplication::UnicodeUTF8));
        Label_GlobalShortcut->setText(QApplication::translate("SettingsDialog", "Global Auto-Type Shortcut:", 0, QApplication::UnicodeUTF8));
        CheckBox_EntryTitlesMatch->setText(QApplication::translate("SettingsDialog", "Use entries' title to match the window for Global Auto-Type", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("SettingsDialog", "You can disable several features of KeePassX here according to your needs in order to keep the user interface slim.", 0, QApplication::UnicodeUTF8));
        CheckBox_FeatureBookmarks->setText(QApplication::translate("SettingsDialog", "Bookmarks", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("SettingsDialog", "Plug-Ins", 0, QApplication::UnicodeUTF8));
        Radio_IntPlugin_None->setText(QApplication::translate("SettingsDialog", "None", 0, QApplication::UnicodeUTF8));
        Radio_IntPlugin_Gnome->setText(QApplication::translate("SettingsDialog", "Gnome Desktop Integration (Gtk 2.x)", 0, QApplication::UnicodeUTF8));
        Radio_IntPlugin_Kde->setText(QApplication::translate("SettingsDialog", "KDE 4 Desktop Integration", 0, QApplication::UnicodeUTF8));
        Label_IntPlugin_Info->setText(QApplication::translate("SettingsDialog", "You need to restart the program before the changes take effect.", 0, QApplication::UnicodeUTF8));
        IntPlugin_Button_Config->setText(QApplication::translate("SettingsDialog", "Configure...", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDLG_H
