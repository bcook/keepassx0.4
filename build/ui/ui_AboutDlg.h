/********************************************************************************
** Form generated from reading UI file 'AboutDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTDLG_H
#define UI_ABOUTDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "../../src/lib/UrlLabel.h"

QT_BEGIN_NAMESPACE

class Ui_AboutDlg
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *vboxLayout1;
    QHBoxLayout *hboxLayout;
    QLabel *labelAppName;
    QLabel *labelAppFunc;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacerItem1;
    QVBoxLayout *vboxLayout2;
    LinkLabel *label_5;
    LinkLabel *label_3;
    QLabel *label_4;
    QWidget *tab_2;
    QVBoxLayout *vboxLayout3;
    QTextEdit *Edit_Thanks;
    QWidget *tab_4;
    QVBoxLayout *vboxLayout4;
    QTextEdit *Edit_Translation;
    QWidget *tab_3;
    QVBoxLayout *vboxLayout5;
    QTextEdit *Edit_License;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *AboutDlg)
    {
        if (AboutDlg->objectName().isEmpty())
            AboutDlg->setObjectName(QString::fromUtf8("AboutDlg"));
        AboutDlg->resize(419, 305);
        vboxLayout = new QVBoxLayout(AboutDlg);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 50, QSizePolicy::Minimum, QSizePolicy::Fixed);

        vboxLayout->addItem(spacerItem);

        tabWidget = new QTabWidget(AboutDlg);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        vboxLayout1 = new QVBoxLayout(tab);
        vboxLayout1->setSpacing(6);
        vboxLayout1->setContentsMargins(11, 11, 11, 11);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        labelAppName = new QLabel(tab);
        labelAppName->setObjectName(QString::fromUtf8("labelAppName"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(labelAppName->sizePolicy().hasHeightForWidth());
        labelAppName->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        labelAppName->setFont(font);

        hboxLayout->addWidget(labelAppName);

        labelAppFunc = new QLabel(tab);
        labelAppFunc->setObjectName(QString::fromUtf8("labelAppFunc"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(labelAppFunc->sizePolicy().hasHeightForWidth());
        labelAppFunc->setSizePolicy(sizePolicy1);
        QFont font1;
        font1.setPointSize(9);
        labelAppFunc->setFont(font1);
        labelAppFunc->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        hboxLayout->addWidget(labelAppFunc);


        vboxLayout1->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setSpacing(6);
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacerItem1 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem1);

        vboxLayout2 = new QVBoxLayout();
        vboxLayout2->setSpacing(6);
        vboxLayout2->setObjectName(QString::fromUtf8("vboxLayout2"));
        label_5 = new LinkLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        vboxLayout2->addWidget(label_5);

        label_3 = new LinkLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        vboxLayout2->addWidget(label_3);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        vboxLayout2->addWidget(label_4);


        hboxLayout1->addLayout(vboxLayout2);


        vboxLayout1->addLayout(hboxLayout1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        vboxLayout3 = new QVBoxLayout(tab_2);
        vboxLayout3->setSpacing(6);
        vboxLayout3->setContentsMargins(11, 11, 11, 11);
        vboxLayout3->setObjectName(QString::fromUtf8("vboxLayout3"));
        Edit_Thanks = new QTextEdit(tab_2);
        Edit_Thanks->setObjectName(QString::fromUtf8("Edit_Thanks"));
        Edit_Thanks->setReadOnly(true);

        vboxLayout3->addWidget(Edit_Thanks);

        tabWidget->addTab(tab_2, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        vboxLayout4 = new QVBoxLayout(tab_4);
        vboxLayout4->setSpacing(6);
        vboxLayout4->setContentsMargins(11, 11, 11, 11);
        vboxLayout4->setObjectName(QString::fromUtf8("vboxLayout4"));
        Edit_Translation = new QTextEdit(tab_4);
        Edit_Translation->setObjectName(QString::fromUtf8("Edit_Translation"));
        Edit_Translation->setReadOnly(true);

        vboxLayout4->addWidget(Edit_Translation);

        tabWidget->addTab(tab_4, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        vboxLayout5 = new QVBoxLayout(tab_3);
        vboxLayout5->setSpacing(6);
        vboxLayout5->setContentsMargins(11, 11, 11, 11);
        vboxLayout5->setObjectName(QString::fromUtf8("vboxLayout5"));
        Edit_License = new QTextEdit(tab_3);
        Edit_License->setObjectName(QString::fromUtf8("Edit_License"));
        Edit_License->setReadOnly(true);

        vboxLayout5->addWidget(Edit_License);

        tabWidget->addTab(tab_3, QString());

        vboxLayout->addWidget(tabWidget);

        ButtonBox = new QDialogButtonBox(AboutDlg);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Close);

        vboxLayout->addWidget(ButtonBox);

        QWidget::setTabOrder(tabWidget, Edit_Thanks);
        QWidget::setTabOrder(Edit_Thanks, Edit_Translation);
        QWidget::setTabOrder(Edit_Translation, Edit_License);
        QWidget::setTabOrder(Edit_License, ButtonBox);

        retranslateUi(AboutDlg);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AboutDlg);
    } // setupUi

    void retranslateUi(QDialog *AboutDlg)
    {
        AboutDlg->setWindowTitle(QApplication::translate("AboutDlg", "About", 0, QApplication::UnicodeUTF8));
        labelAppName->setText(QApplication::translate("AboutDlg", "AppName", 0, QApplication::UnicodeUTF8));
        labelAppFunc->setText(QApplication::translate("AboutDlg", "AppFunc", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("AboutDlg", "http://keepassx.sourceforge.net", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("AboutDlg", "keepassx@gmail.com", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("AboutDlg", "Copyright (C) 2005 - 2009 KeePassX Team\n"
"KeePassX is distributed under the terms of the\n"
"General Public License (GPL) version 2.", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("AboutDlg", "About", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("AboutDlg", "Credits", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("AboutDlg", "Translation", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("AboutDlg", "License", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class AboutDlg: public Ui_AboutDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTDLG_H
