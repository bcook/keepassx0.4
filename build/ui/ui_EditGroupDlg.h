/********************************************************************************
** Form generated from reading UI file 'EditGroupDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITGROUPDLG_H
#define UI_EDITGROUPDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_EditGroupDialog
{
public:
    QVBoxLayout *vboxLayout;
    QGridLayout *gridLayout;
    QLabel *Label2;
    QLabel *Label1;
    QComboBox *ComboIconPicker;
    QSpacerItem *spacerItem;
    QLineEdit *EditTitle;
    QToolButton *Button_Icon;
    QFrame *line1;
    QDialogButtonBox *ButtonBox;

    void setupUi(QDialog *EditGroupDialog)
    {
        if (EditGroupDialog->objectName().isEmpty())
            EditGroupDialog->setObjectName(QString::fromUtf8("EditGroupDialog"));
        EditGroupDialog->resize(350, 120);
        EditGroupDialog->setMinimumSize(QSize(350, 0));
        vboxLayout = new QVBoxLayout(EditGroupDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Label2 = new QLabel(EditGroupDialog);
        Label2->setObjectName(QString::fromUtf8("Label2"));

        gridLayout->addWidget(Label2, 1, 0, 1, 1);

        Label1 = new QLabel(EditGroupDialog);
        Label1->setObjectName(QString::fromUtf8("Label1"));

        gridLayout->addWidget(Label1, 0, 0, 1, 1);

        ComboIconPicker = new QComboBox(EditGroupDialog);
        ComboIconPicker->setObjectName(QString::fromUtf8("ComboIconPicker"));

        gridLayout->addWidget(ComboIconPicker, 1, 1, 1, 1);

        spacerItem = new QSpacerItem(172, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(spacerItem, 1, 3, 1, 1);

        EditTitle = new QLineEdit(EditGroupDialog);
        EditTitle->setObjectName(QString::fromUtf8("EditTitle"));

        gridLayout->addWidget(EditTitle, 0, 1, 1, 3);

        Button_Icon = new QToolButton(EditGroupDialog);
        Button_Icon->setObjectName(QString::fromUtf8("Button_Icon"));

        gridLayout->addWidget(Button_Icon, 1, 2, 1, 1);


        vboxLayout->addLayout(gridLayout);

        line1 = new QFrame(EditGroupDialog);
        line1->setObjectName(QString::fromUtf8("line1"));
        line1->setFrameShape(QFrame::HLine);
        line1->setFrameShadow(QFrame::Sunken);
        line1->setFrameShape(QFrame::HLine);

        vboxLayout->addWidget(line1);

        ButtonBox = new QDialogButtonBox(EditGroupDialog);
        ButtonBox->setObjectName(QString::fromUtf8("ButtonBox"));
        ButtonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(ButtonBox);

        QWidget::setTabOrder(EditTitle, ComboIconPicker);
        QWidget::setTabOrder(ComboIconPicker, Button_Icon);
        QWidget::setTabOrder(Button_Icon, ButtonBox);

        retranslateUi(EditGroupDialog);

        QMetaObject::connectSlotsByName(EditGroupDialog);
    } // setupUi

    void retranslateUi(QDialog *EditGroupDialog)
    {
        EditGroupDialog->setWindowTitle(QApplication::translate("EditGroupDialog", "Group Properties", 0, QApplication::UnicodeUTF8));
        Label2->setText(QApplication::translate("EditGroupDialog", "Icon:", 0, QApplication::UnicodeUTF8));
        Label1->setText(QApplication::translate("EditGroupDialog", "Title:", 0, QApplication::UnicodeUTF8));
        Button_Icon->setText(QApplication::translate("EditGroupDialog", ">", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class EditGroupDialog: public Ui_EditGroupDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITGROUPDLG_H
