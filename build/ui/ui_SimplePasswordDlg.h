/********************************************************************************
** Form generated from reading UI file 'SimplePasswordDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIMPLEPASSWORDDLG_H
#define UI_SIMPLEPASSWORDDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SimplePasswordDialog
{
public:
    QVBoxLayout *vboxLayout;
    QHBoxLayout *hboxLayout;
    QLabel *textLabel1;
    QLineEdit *EditPassword;
    QPushButton *Button_HidePassword;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SimplePasswordDialog)
    {
        if (SimplePasswordDialog->objectName().isEmpty())
            SimplePasswordDialog->setObjectName(QString::fromUtf8("SimplePasswordDialog"));
        SimplePasswordDialog->resize(345, 90);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SimplePasswordDialog->sizePolicy().hasHeightForWidth());
        SimplePasswordDialog->setSizePolicy(sizePolicy);
        SimplePasswordDialog->setMinimumSize(QSize(345, 90));
        SimplePasswordDialog->setMaximumSize(QSize(16777215, 90));
        vboxLayout = new QVBoxLayout(SimplePasswordDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setContentsMargins(11, 11, 11, 11);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(6);
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        textLabel1 = new QLabel(SimplePasswordDialog);
        textLabel1->setObjectName(QString::fromUtf8("textLabel1"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textLabel1->sizePolicy().hasHeightForWidth());
        textLabel1->setSizePolicy(sizePolicy1);

        hboxLayout->addWidget(textLabel1);

        EditPassword = new QLineEdit(SimplePasswordDialog);
        EditPassword->setObjectName(QString::fromUtf8("EditPassword"));
        QFont font;
        font.setFamily(QString::fromUtf8("Serif"));
        EditPassword->setFont(font);

        hboxLayout->addWidget(EditPassword);

        Button_HidePassword = new QPushButton(SimplePasswordDialog);
        Button_HidePassword->setObjectName(QString::fromUtf8("Button_HidePassword"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Button_HidePassword->sizePolicy().hasHeightForWidth());
        Button_HidePassword->setSizePolicy(sizePolicy2);
        Button_HidePassword->setMinimumSize(QSize(23, 23));
        Button_HidePassword->setMaximumSize(QSize(23, 23));
        Button_HidePassword->setCheckable(true);

        hboxLayout->addWidget(Button_HidePassword);


        vboxLayout->addLayout(hboxLayout);

        buttonBox = new QDialogButtonBox(SimplePasswordDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);

        QWidget::setTabOrder(EditPassword, Button_HidePassword);
        QWidget::setTabOrder(Button_HidePassword, buttonBox);

        retranslateUi(SimplePasswordDialog);

        QMetaObject::connectSlotsByName(SimplePasswordDialog);
    } // setupUi

    void retranslateUi(QDialog *SimplePasswordDialog)
    {
        SimplePasswordDialog->setWindowTitle(QApplication::translate("SimplePasswordDialog", "Enter your Password", 0, QApplication::UnicodeUTF8));
        textLabel1->setText(QApplication::translate("SimplePasswordDialog", "Password:", 0, QApplication::UnicodeUTF8));
        Button_HidePassword->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SimplePasswordDialog: public Ui_SimplePasswordDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIMPLEPASSWORDDLG_H
