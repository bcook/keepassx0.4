/********************************************************************************
** Form generated from reading UI file 'WorkspaceLockedWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORKSPACELOCKEDWIDGET_H
#define UI_WORKSPACELOCKEDWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_WorkspaceLockedWidget
{
public:
    QVBoxLayout *vboxLayout;
    QSpacerItem *spacerItem;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem1;
    QLabel *label;
    QSpacerItem *spacerItem2;
    QHBoxLayout *hboxLayout1;
    QSpacerItem *spacerItem3;
    QVBoxLayout *vboxLayout1;
    QPushButton *Button_Unlock;
    QPushButton *Button_CloseDatabase;
    QSpacerItem *spacerItem4;
    QSpacerItem *spacerItem5;

    void setupUi(QWidget *WorkspaceLockedWidget)
    {
        if (WorkspaceLockedWidget->objectName().isEmpty())
            WorkspaceLockedWidget->setObjectName(QString::fromUtf8("WorkspaceLockedWidget"));
        WorkspaceLockedWidget->resize(400, 300);
        vboxLayout = new QVBoxLayout(WorkspaceLockedWidget);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        spacerItem = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        spacerItem1 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem1);

        label = new QLabel(WorkspaceLockedWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        hboxLayout->addWidget(label);

        spacerItem2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem2);


        vboxLayout->addLayout(hboxLayout);

        hboxLayout1 = new QHBoxLayout();
        hboxLayout1->setObjectName(QString::fromUtf8("hboxLayout1"));
        spacerItem3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem3);

        vboxLayout1 = new QVBoxLayout();
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        Button_Unlock = new QPushButton(WorkspaceLockedWidget);
        Button_Unlock->setObjectName(QString::fromUtf8("Button_Unlock"));

        vboxLayout1->addWidget(Button_Unlock);

        Button_CloseDatabase = new QPushButton(WorkspaceLockedWidget);
        Button_CloseDatabase->setObjectName(QString::fromUtf8("Button_CloseDatabase"));

        vboxLayout1->addWidget(Button_CloseDatabase);


        hboxLayout1->addLayout(vboxLayout1);

        spacerItem4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout1->addItem(spacerItem4);


        vboxLayout->addLayout(hboxLayout1);

        spacerItem5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(spacerItem5);


        retranslateUi(WorkspaceLockedWidget);

        QMetaObject::connectSlotsByName(WorkspaceLockedWidget);
    } // setupUi

    void retranslateUi(QWidget *WorkspaceLockedWidget)
    {
        WorkspaceLockedWidget->setWindowTitle(QApplication::translate("WorkspaceLockedWidget", "Form", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("WorkspaceLockedWidget", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Verdana'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt; font-weight:600;\">The workspace is locked.</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        Button_Unlock->setText(QApplication::translate("WorkspaceLockedWidget", "Unlock", 0, QApplication::UnicodeUTF8));
        Button_CloseDatabase->setText(QApplication::translate("WorkspaceLockedWidget", "Close Database", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class WorkspaceLockedWidget: public Ui_WorkspaceLockedWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORKSPACELOCKEDWIDGET_H
