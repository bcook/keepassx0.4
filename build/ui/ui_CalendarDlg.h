/********************************************************************************
** Form generated from reading UI file 'CalendarDlg.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALENDARDLG_H
#define UI_CALENDARDLG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCalendarWidget>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CalendarDialog
{
public:
    QVBoxLayout *vboxLayout;
    QCalendarWidget *calendarWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CalendarDialog)
    {
        if (CalendarDialog->objectName().isEmpty())
            CalendarDialog->setObjectName(QString::fromUtf8("CalendarDialog"));
        CalendarDialog->resize(334, 238);
        vboxLayout = new QVBoxLayout(CalendarDialog);
        vboxLayout->setSpacing(6);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        calendarWidget = new QCalendarWidget(CalendarDialog);
        calendarWidget->setObjectName(QString::fromUtf8("calendarWidget"));

        vboxLayout->addWidget(calendarWidget);

        buttonBox = new QDialogButtonBox(CalendarDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::NoButton|QDialogButtonBox::Ok);

        vboxLayout->addWidget(buttonBox);


        retranslateUi(CalendarDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CalendarDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CalendarDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CalendarDialog);
    } // setupUi

    void retranslateUi(QDialog *CalendarDialog)
    {
        CalendarDialog->setWindowTitle(QApplication::translate("CalendarDialog", "Calendar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CalendarDialog: public Ui_CalendarDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALENDARDLG_H
